<?php

class MyTestRss {
        function vtlib_handler($moduleName, $eventType) {
                if ($eventType == 'module.postinstall') {
                        $data = array('url' => 'https://www.vtiger.com/blogs/?feed=rss2',
                                    'title' => 'Vtiger Blogs');
                                MyTestRss_Record_Model::create($data);
                }
        }
}