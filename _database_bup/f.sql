CREATE TABLE vtiger_users (
  id INTEGER NOT NULL AUTO_INCREMENT,
  user_name VARCHAR(255),
  user_password VARCHAR(200),
  user_hash VARCHAR(32),
  cal_color VARCHAR(25) DEFAULT '#E6FAD8',
  first_name VARCHAR(30),
  last_name VARCHAR(30),
  reports_to_id VARCHAR(36),
  is_admin VARCHAR(3) DEFAULT '0',
  currency_id INTEGER(19) NOT NULL DEFAULT 1,
  description TEXT,
  date_entered TIMESTAMP NOT NULL,
  date_modified DATETIME,
  modified_user_id VARCHAR(36),
  title VARCHAR(50),
  department VARCHAR(50),
  phone_home VARCHAR(50),
  phone_mobile VARCHAR(50),
  phone_work VARCHAR(50),
  phone_other VARCHAR(50),
  phone_fax VARCHAR(50),
  email1 VARCHAR(100),
  email2 VARCHAR(100),
  secondaryemail VARCHAR(100),
  status VARCHAR(25),
  signature VARCHAR(1000),
  address_street VARCHAR(150),
  address_city VARCHAR(100),
  address_state VARCHAR(100),
  address_country VARCHAR(25),
  address_postalcode VARCHAR(9),
  user_preferences TEXT,
  tz VARCHAR(30),
  holidays VARCHAR(60),
  namedays VARCHAR(60),
  workdays VARCHAR(30),
  weekstart INTEGER(11),
  date_format VARCHAR(200),
  hour_format VARCHAR(30) DEFAULT 'am/pm',
  start_hour VARCHAR(30) DEFAULT '10:00',
  end_hour VARCHAR(30) DEFAULT '23:00',
  is_owner VARCHAR(100) DEFAULT '0',
  activity_view VARCHAR(200) DEFAULT 'Today',
  lead_view VARCHAR(200) DEFAULT 'Today',
  imagename VARCHAR(250),
  deleted INTEGER(1) NOT NULL DEFAULT 0,
  confirm_password VARCHAR(300),
  internal_mailer VARCHAR(3) NOT NULL DEFAULT '1',
  reminder_interval VARCHAR(100),
  reminder_next_time VARCHAR(100),
  crypt_type VARCHAR(20) NOT NULL DEFAULT 'MD5',
  accesskey VARCHAR(36),
  theme VARCHAR(100),
  language VARCHAR(36),
  time_zone VARCHAR(200),
  currency_grouping_pattern VARCHAR(100),
  currency_decimal_separator VARCHAR(2),
  currency_grouping_separator VARCHAR(2),
  currency_symbol_placement VARCHAR(20),
  PRIMARY KEY (id)
) Engine = InnoDB;
ALTER TABLE
  vtiger_users
ADD
  INDEX user_user_name_idx (user_name);
ALTER TABLE
  vtiger_users
ADD
  INDEX user_user_password_idx (user_password);CREATE TABLE vtiger_crmentity (
    crmid INTEGER(19) NOT NULL,
    smcreatorid INTEGER(19) NOT NULL DEFAULT 0,
    smownerid INTEGER(19) NOT NULL DEFAULT 0,
    modifiedby INTEGER(19) NOT NULL DEFAULT 0,
    setype VARCHAR(30) NOT NULL,
    description TEXT,
    createdtime DATETIME NOT NULL,
    modifiedtime DATETIME NOT NULL,
    viewedtime DATETIME,
    status VARCHAR(50),
    version INTEGER(19) NOT NULL DEFAULT 0,
    presence INTEGER(1) DEFAULT 1,
    deleted INTEGER(1) NOT NULL DEFAULT 0,
    smgroupid INTEGER(19),
    PRIMARY KEY (crmid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_crmentity
ADD
  INDEX crmentity_smcreatorid_idx (smcreatorid);
ALTER TABLE
  vtiger_crmentity
ADD
  INDEX crmentity_modifiedby_idx (modifiedby);
ALTER TABLE
  vtiger_crmentity
ADD
  INDEX crmentity_deleted_idx (deleted);
ALTER TABLE
  vtiger_crmentity
ADD
  INDEX crm_ownerid_del_setype_idx (smownerid, deleted, setype);CREATE TABLE vtiger_sharedcalendar (
    userid INTEGER(19) NOT NULL,
    sharedid INTEGER(19) NOT NULL,
    PRIMARY KEY (userid, sharedid)
  ) Engine = InnoDB;CREATE TABLE vtiger_tab (
    tabid INTEGER(19) NOT NULL DEFAULT 0,
    name VARCHAR(25) NOT NULL,
    presence INTEGER(19) NOT NULL DEFAULT 1,
    tabsequence INTEGER(10),
    tablabel VARCHAR(25) NOT NULL,
    modifiedby INTEGER(19),
    modifiedtime INTEGER(19),
    customized INTEGER(19),
    ownedby INTEGER(19),
    isentitytype INTEGER NOT NULL DEFAULT 1,
    trial INTEGER(1) NOT NULL DEFAULT 0,
    version VARCHAR(10),
    parent VARCHAR(30),
    source VARCHAR(255) DEFAULT 'custom',
    PRIMARY KEY (tabid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_tab
ADD
  UNIQUE INDEX tab_name_idx (name);
ALTER TABLE
  vtiger_tab
ADD
  INDEX tab_modifiedby_idx (modifiedby);
ALTER TABLE
  vtiger_tab
ADD
  INDEX tab_tabid_idx (tabid);CREATE TABLE vtiger_blocks (
    blockid INTEGER(19) NOT NULL,
    tabid INTEGER(19) NOT NULL,
    blocklabel VARCHAR(100) NOT NULL,
    sequence INTEGER(10),
    show_title INTEGER(2),
    visible INTEGER(2) NOT NULL DEFAULT 0,
    create_view INTEGER(2) NOT NULL DEFAULT 0,
    edit_view INTEGER(2) NOT NULL DEFAULT 0,
    detail_view INTEGER(2) NOT NULL DEFAULT 0,
    display_status INTEGER(1) NOT NULL DEFAULT 1,
    iscustom INTEGER(1) NOT NULL DEFAULT 0,
    PRIMARY KEY (blockid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_blocks
ADD
  INDEX block_tabid_idx (tabid);
ALTER TABLE
  vtiger_blocks
ADD
  CONSTRAINT fk_1_vtiger_blocks FOREIGN KEY (tabid) REFERENCES vtiger_tab(tabid) ON DELETE CASCADE;CREATE TABLE vtiger_field (
    tabid INTEGER(19) NOT NULL,
    fieldid INTEGER(19) NOT NULL AUTO_INCREMENT,
    columnname VARCHAR(30) NOT NULL,
    tablename VARCHAR(50) NOT NULL,
    generatedtype INTEGER(19) NOT NULL DEFAULT 0,
    uitype VARCHAR(30) NOT NULL,
    fieldname VARCHAR(50) NOT NULL,
    fieldlabel VARCHAR(50) NOT NULL,
    readonly INTEGER(1) NOT NULL,
    presence INTEGER(19) NOT NULL DEFAULT 1,
    defaultvalue TEXT,
    maximumlength INTEGER(19),
    sequence INTEGER(19),
    block INTEGER(19),
    displaytype INTEGER(19),
    typeofdata VARCHAR(100),
    quickcreate INTEGER(10) NOT NULL DEFAULT 1,
    quickcreatesequence INTEGER(19),
    info_type VARCHAR(20),
    masseditable INTEGER(10) NOT NULL DEFAULT 1,
    PRIMARY KEY (fieldid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_field
ADD
  INDEX field_tabid_idx (tabid);
ALTER TABLE
  vtiger_field
ADD
  INDEX field_fieldname_idx (fieldname);
ALTER TABLE
  vtiger_field
ADD
  INDEX field_block_idx (block);
ALTER TABLE
  vtiger_field
ADD
  INDEX field_displaytype_idx (displaytype);
ALTER TABLE
  vtiger_field
ADD
  CONSTRAINT fk_1_vtiger_field FOREIGN KEY (tabid) REFERENCES vtiger_tab(tabid) ON DELETE CASCADE;CREATE TABLE vtiger_account (
    accountid INTEGER(19) NOT NULL DEFAULT 0,
    account_no VARCHAR(100) NOT NULL,
    accountname VARCHAR(100) NOT NULL,
    parentid INTEGER(19) DEFAULT 0,
    account_type VARCHAR(200),
    industry VARCHAR(200),
    annualrevenue INTEGER(19) DEFAULT 0,
    rating VARCHAR(200),
    ownership VARCHAR(50),
    siccode VARCHAR(50),
    tickersymbol VARCHAR(30),
    phone VARCHAR(30),
    otherphone VARCHAR(30),
    email1 VARCHAR(100),
    email2 VARCHAR(100),
    website VARCHAR(100),
    fax VARCHAR(30),
    employees INTEGER(10) DEFAULT 0,
    emailoptout VARCHAR(3) DEFAULT '0',
    notify_owner VARCHAR(3) DEFAULT '0',
    PRIMARY KEY (accountid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_account
ADD
  INDEX account_account_type_idx (account_type);
ALTER TABLE
  vtiger_account
ADD
  CONSTRAINT fk_1_vtiger_account FOREIGN KEY (accountid) REFERENCES vtiger_crmentity(crmid) ON DELETE CASCADE;CREATE TABLE vtiger_accountbillads (
    accountaddressid INTEGER(19) NOT NULL DEFAULT 0,
    bill_city VARCHAR(30),
    bill_code VARCHAR(30),
    bill_country VARCHAR(30),
    bill_state VARCHAR(30),
    bill_street VARCHAR(250),
    bill_pobox VARCHAR(30),
    PRIMARY KEY (accountaddressid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_accountbillads
ADD
  CONSTRAINT fk_1_vtiger_accountbillads FOREIGN KEY (accountaddressid) REFERENCES vtiger_account(accountid) ON DELETE CASCADE;CREATE TABLE vtiger_accountshipads (
    accountaddressid INTEGER(19) NOT NULL DEFAULT 0,
    ship_city VARCHAR(30),
    ship_code VARCHAR(30),
    ship_country VARCHAR(30),
    ship_state VARCHAR(30),
    ship_pobox VARCHAR(30),
    ship_street VARCHAR(250),
    PRIMARY KEY (accountaddressid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_accountshipads
ADD
  CONSTRAINT fk_1_vtiger_accountshipads FOREIGN KEY (accountaddressid) REFERENCES vtiger_account(accountid) ON DELETE CASCADE;CREATE TABLE vtiger_accountscf (
    accountid INTEGER(19) NOT NULL DEFAULT 0,
    PRIMARY KEY (accountid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_accountscf
ADD
  CONSTRAINT fk_1_vtiger_accountscf FOREIGN KEY (accountid) REFERENCES vtiger_account(accountid) ON DELETE CASCADE;CREATE TABLE vtiger_contactdetails (
    contactid INTEGER(19) NOT NULL DEFAULT 0,
    contact_no VARCHAR(100) NOT NULL,
    accountid INTEGER(19),
    salutation VARCHAR(200),
    firstname VARCHAR(40),
    lastname VARCHAR(80) NOT NULL,
    email VARCHAR(100),
    phone VARCHAR(50),
    mobile VARCHAR(50),
    title VARCHAR(50),
    department VARCHAR(30),
    fax VARCHAR(50),
    reportsto VARCHAR(30),
    training VARCHAR(50),
    usertype VARCHAR(50),
    contacttype VARCHAR(50),
    otheremail VARCHAR(100),
    secondaryemail VARCHAR(100),
    donotcall VARCHAR(3),
    emailoptout VARCHAR(3) DEFAULT '0',
    imagename VARCHAR(150),
    reference VARCHAR(3),
    notify_owner VARCHAR(3) DEFAULT '0',
    PRIMARY KEY (contactid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_contactdetails
ADD
  INDEX contactdetails_accountid_idx (accountid);
ALTER TABLE
  vtiger_contactdetails
ADD
  CONSTRAINT fk_1_vtiger_contactdetails FOREIGN KEY (contactid) REFERENCES vtiger_crmentity(crmid) ON DELETE CASCADE;CREATE TABLE vtiger_contactsubdetails (
    contactsubscriptionid INTEGER(19) NOT NULL DEFAULT 0,
    homephone VARCHAR(50),
    otherphone VARCHAR(50),
    assistant VARCHAR(30),
    assistantphone VARCHAR(50),
    birthday DATE,
    laststayintouchrequest INTEGER(30) DEFAULT 0,
    laststayintouchsavedate INTEGER(19) DEFAULT 0,
    leadsource VARCHAR(200),
    PRIMARY KEY (contactsubscriptionid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_contactsubdetails
ADD
  CONSTRAINT fk_1_vtiger_contactsubdetails FOREIGN KEY (contactsubscriptionid) REFERENCES vtiger_contactdetails(contactid) ON DELETE CASCADE;CREATE TABLE vtiger_contactaddress (
    contactaddressid INTEGER(19) NOT NULL DEFAULT 0,
    mailingcity VARCHAR(40),
    mailingstreet VARCHAR(250),
    mailingcountry VARCHAR(40),
    othercountry VARCHAR(30),
    mailingstate VARCHAR(30),
    mailingpobox VARCHAR(30),
    othercity VARCHAR(40),
    otherstate VARCHAR(50),
    mailingzip VARCHAR(30),
    otherzip VARCHAR(30),
    otherstreet VARCHAR(250),
    otherpobox VARCHAR(30),
    PRIMARY KEY (contactaddressid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_contactaddress
ADD
  CONSTRAINT fk_1_vtiger_contactaddress FOREIGN KEY (contactaddressid) REFERENCES vtiger_contactdetails(contactid) ON DELETE CASCADE;CREATE TABLE vtiger_contactscf (
    contactid INTEGER(19) NOT NULL DEFAULT 0,
    PRIMARY KEY (contactid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_contactscf
ADD
  CONSTRAINT fk_1_vtiger_contactscf FOREIGN KEY (contactid) REFERENCES vtiger_contactdetails(contactid) ON DELETE CASCADE;CREATE TABLE vtiger_portalinfo (
    id INTEGER(11) NOT NULL,
    user_name VARCHAR(50),
    user_password VARCHAR(255),
    type VARCHAR(5),
    cryptmode VARCHAR(20),
    last_login_time DATETIME,
    login_time DATETIME,
    logout_time DATETIME,
    isactive INTEGER(1),
    PRIMARY KEY (id)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_portalinfo
ADD
  CONSTRAINT fk_1_vtiger_portalinfo FOREIGN KEY (id) REFERENCES vtiger_contactdetails(contactid) ON DELETE CASCADE;CREATE TABLE vtiger_customerdetails (
    customerid INTEGER(19) NOT NULL,
    portal VARCHAR(3),
    support_start_date DATE,
    support_end_date DATE,
    PRIMARY KEY (customerid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_customerdetails
ADD
  CONSTRAINT fk_1_vtiger_customerdetails FOREIGN KEY (customerid) REFERENCES vtiger_contactdetails(contactid) ON DELETE CASCADE;CREATE TABLE vtiger_leaddetails (
    leadid INTEGER(19) NOT NULL,
    lead_no VARCHAR(100) NOT NULL,
    email VARCHAR(100),
    interest VARCHAR(50),
    firstname VARCHAR(40),
    salutation VARCHAR(200),
    lastname VARCHAR(80) NOT NULL,
    company VARCHAR(100) NOT NULL,
    annualrevenue INTEGER(19) DEFAULT 0,
    industry VARCHAR(200),
    campaign VARCHAR(30),
    rating VARCHAR(200),
    leadstatus VARCHAR(50),
    leadsource VARCHAR(200),
    converted INTEGER(1) DEFAULT 0,
    designation VARCHAR(50) DEFAULT 'SalesMan',
    licencekeystatus VARCHAR(50),
    space VARCHAR(250),
    comments TEXT,
    priority VARCHAR(50),
    demorequest VARCHAR(50),
    partnercontact VARCHAR(50),
    productversion VARCHAR(20),
    product VARCHAR(50),
    maildate DATE,
    nextstepdate DATE,
    fundingsituation VARCHAR(50),
    purpose VARCHAR(50),
    evaluationstatus VARCHAR(50),
    transferdate DATE,
    revenuetype VARCHAR(50),
    noofemployees INTEGER(50),
    secondaryemail VARCHAR(100),
    assignleadchk INTEGER(1) DEFAULT 0,
    PRIMARY KEY (leadid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_leaddetails
ADD
  INDEX leaddetails_converted_leadstatus_idx (converted, leadstatus);
ALTER TABLE
  vtiger_leaddetails
ADD
  CONSTRAINT fk_1_vtiger_leaddetails FOREIGN KEY (leadid) REFERENCES vtiger_crmentity(crmid) ON DELETE CASCADE;CREATE TABLE vtiger_leadsubdetails (
    leadsubscriptionid INTEGER(19) NOT NULL DEFAULT 0,
    website VARCHAR(255),
    callornot INTEGER(1) DEFAULT 0,
    readornot INTEGER(1) DEFAULT 0,
    empct INTEGER(10) DEFAULT 0,
    PRIMARY KEY (leadsubscriptionid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_leadsubdetails
ADD
  CONSTRAINT fk_1_vtiger_leadsubdetails FOREIGN KEY (leadsubscriptionid) REFERENCES vtiger_leaddetails(leadid) ON DELETE CASCADE;CREATE TABLE vtiger_leadaddress (
    leadaddressid INTEGER(19) NOT NULL DEFAULT 0,
    city VARCHAR(30),
    code VARCHAR(30),
    state VARCHAR(30),
    pobox VARCHAR(30),
    country VARCHAR(30),
    phone VARCHAR(50),
    mobile VARCHAR(50),
    fax VARCHAR(50),
    lane VARCHAR(250),
    leadaddresstype VARCHAR(30) DEFAULT 'Billing',
    PRIMARY KEY (leadaddressid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_leadaddress
ADD
  CONSTRAINT fk_1_vtiger_leadaddress FOREIGN KEY (leadaddressid) REFERENCES vtiger_leaddetails(leadid) ON DELETE CASCADE;CREATE TABLE vtiger_leadscf (
    leadid INTEGER(19) NOT NULL DEFAULT 0,
    PRIMARY KEY (leadid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_leadscf
ADD
  CONSTRAINT fk_1_vtiger_leadscf FOREIGN KEY (leadid) REFERENCES vtiger_leaddetails(leadid) ON DELETE CASCADE;CREATE TABLE vtiger_notes (
    notesid INTEGER(19) NOT NULL DEFAULT 0,
    note_no VARCHAR(100) NOT NULL,
    title VARCHAR(50) NOT NULL,
    filename VARCHAR(200),
    notecontent TEXT,
    folderid INTEGER(19) NOT NULL DEFAULT 1,
    filetype VARCHAR(50) DEFAULT NULL,
    filelocationtype VARCHAR(5) DEFAULT NULL,
    filedownloadcount INTEGER(19) DEFAULT NULL,
    filestatus INTEGER(19) DEFAULT NULL,
    filesize INTEGER(19) NOT NULL DEFAULT 0,
    fileversion VARCHAR(50) DEFAULT NULL,
    PRIMARY KEY (notesid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_notes
ADD
  INDEX notes_title_idx (title);
ALTER TABLE
  vtiger_notes
ADD
  INDEX notes_notesid_idx (notesid);
ALTER TABLE
  vtiger_notes
ADD
  CONSTRAINT fk_1_vtiger_notes FOREIGN KEY (notesid) REFERENCES vtiger_crmentity(crmid) ON DELETE CASCADE;CREATE TABLE vtiger_potential (
    potentialid INTEGER(19) NOT NULL DEFAULT 0,
    potential_no VARCHAR(100) NOT NULL,
    related_to INTEGER(19),
    potentialname VARCHAR(120) NOT NULL,
    amount NUMERIC(14, 2) DEFAULT 0,
    currency VARCHAR(20),
    closingdate DATE,
    typeofrevenue VARCHAR(50),
    nextstep VARCHAR(100),
    private INTEGER(1) DEFAULT 0,
    probability NUMERIC(7, 3) DEFAULT 0,
    campaignid INTEGER(19),
    sales_stage VARCHAR(200),
    potentialtype VARCHAR(200),
    leadsource VARCHAR(200),
    productid INTEGER(50),
    productversion VARCHAR(50),
    quotationref VARCHAR(50),
    partnercontact VARCHAR(50),
    remarks VARCHAR(50),
    runtimefee INTEGER(19) DEFAULT 0,
    followupdate DATE,
    evaluationstatus VARCHAR(50),
    description TEXT,
    forecastcategory INTEGER(19) DEFAULT 0,
    outcomeanalysis INTEGER(19) DEFAULT 0,
    PRIMARY KEY (potentialid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_potential
ADD
  INDEX potential_relatedto_idx (related_to);
ALTER TABLE
  vtiger_potential
ADD
  INDEX potentail_sales_stage_idx (sales_stage);
ALTER TABLE
  vtiger_potential
ADD
  INDEX potentail_sales_stage_amount_idx (amount, sales_stage);
ALTER TABLE
  vtiger_potential
ADD
  CONSTRAINT fk_1_vtiger_potential FOREIGN KEY (potentialid) REFERENCES vtiger_crmentity(crmid) ON DELETE CASCADE;CREATE TABLE vtiger_potstagehistory (
    historyid INTEGER(19) NOT NULL AUTO_INCREMENT,
    potentialid INTEGER(19) NOT NULL,
    amount NUMERIC,
    stage VARCHAR(100),
    probability NUMERIC(7, 3),
    expectedrevenue NUMERIC,
    closedate DATE,
    lastmodified DATETIME,
    PRIMARY KEY (historyid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_potstagehistory
ADD
  INDEX potstagehistory_potentialid_idx (potentialid);
ALTER TABLE
  vtiger_potstagehistory
ADD
  CONSTRAINT fk_1_vtiger_potstagehistory FOREIGN KEY (potentialid) REFERENCES vtiger_potential(potentialid) ON DELETE CASCADE;CREATE TABLE vtiger_potentialscf (
    potentialid INTEGER(19) NOT NULL DEFAULT 0,
    PRIMARY KEY (potentialid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_potentialscf
ADD
  CONSTRAINT fk_1_vtiger_potentialscf FOREIGN KEY (potentialid) REFERENCES vtiger_potential(potentialid) ON DELETE CASCADE;CREATE TABLE vtiger_activity (
    activityid INTEGER(19) NOT NULL DEFAULT 0,
    subject VARCHAR(100) NOT NULL,
    semodule VARCHAR(20),
    activitytype VARCHAR(200) NOT NULL,
    date_start DATE NOT NULL,
    due_date DATE,
    time_start VARCHAR(50),
    time_end VARCHAR(50),
    sendnotification VARCHAR(3) NOT NULL DEFAULT '0',
    duration_hours VARCHAR(200),
    duration_minutes VARCHAR(200),
    status VARCHAR(200),
    eventstatus VARCHAR(200),
    priority VARCHAR(200),
    location VARCHAR(150),
    notime VARCHAR(3) NOT NULL DEFAULT '0',
    visibility VARCHAR(50) NOT NULL DEFAULT 'all',
    recurringtype VARCHAR(200),
    PRIMARY KEY (activityid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_activity
ADD
  INDEX activity_activityid_subject_idx (activityid, subject);
ALTER TABLE
  vtiger_activity
ADD
  INDEX activity_activitytype_date_start_idx (activitytype, date_start);
ALTER TABLE
  vtiger_activity
ADD
  INDEX activity_date_start_due_date_idx (date_start, due_date);
ALTER TABLE
  vtiger_activity
ADD
  INDEX activity_date_start_time_start_idx (date_start, time_start);
ALTER TABLE
  vtiger_activity
ADD
  INDEX activity_eventstatus_idx (eventstatus);
ALTER TABLE
  vtiger_activity
ADD
  INDEX activity_status_idx (status);
ALTER TABLE
  vtiger_activity
ADD
  CONSTRAINT fk_1_vtiger_activity FOREIGN KEY (activityid) REFERENCES vtiger_crmentity(crmid) ON DELETE CASCADE;CREATE TABLE vtiger_attachments (
    attachmentsid INTEGER(19) NOT NULL,
    name VARCHAR(255) NOT NULL,
    description TEXT,
    type VARCHAR(100),
    path TEXT,
    subject VARCHAR(255),
    PRIMARY KEY (attachmentsid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_attachments
ADD
  INDEX attachments_attachmentsid_idx (attachmentsid);
ALTER TABLE
  vtiger_attachments
ADD
  CONSTRAINT fk_1_vtiger_attachments FOREIGN KEY (attachmentsid) REFERENCES vtiger_crmentity(crmid) ON DELETE CASCADE;CREATE TABLE vtiger_seattachmentsrel (
    crmid INTEGER(19) NOT NULL DEFAULT 0,
    attachmentsid INTEGER(19) NOT NULL DEFAULT 0,
    PRIMARY KEY (crmid, attachmentsid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_seattachmentsrel
ADD
  INDEX seattachmentsrel_attachmentsid_idx (attachmentsid);
ALTER TABLE
  vtiger_seattachmentsrel
ADD
  INDEX seattachmentsrel_crmid_idx (crmid);
ALTER TABLE
  vtiger_seattachmentsrel
ADD
  INDEX seattachmentsrel_attachmentsid_crmid_idx (attachmentsid, crmid);
ALTER TABLE
  vtiger_seattachmentsrel
ADD
  CONSTRAINT fk_2_vtiger_seattachmentsrel FOREIGN KEY (crmid) REFERENCES vtiger_crmentity(crmid) ON DELETE CASCADE;CREATE TABLE vtiger_tracker (
    id INTEGER(11) NOT NULL AUTO_INCREMENT,
    user_id VARCHAR(36),
    module_name VARCHAR(25),
    item_id VARCHAR(36),
    item_summary VARCHAR(255),
    PRIMARY KEY (id)
  ) Engine = InnoDB;CREATE TABLE vtiger_vendor (
    vendorid INTEGER(19) NOT NULL DEFAULT 0,
    vendor_no VARCHAR(100) NOT NULL,
    vendorname VARCHAR(100),
    phone VARCHAR(100),
    email VARCHAR(100),
    website VARCHAR(100),
    glacct VARCHAR(200),
    category VARCHAR(50),
    street TEXT,
    city VARCHAR(30),
    state VARCHAR(30),
    pobox VARCHAR(30),
    postalcode VARCHAR(100),
    country VARCHAR(100),
    description TEXT,
    PRIMARY KEY (vendorid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_vendor
ADD
  CONSTRAINT fk_1_vtiger_vendor FOREIGN KEY (vendorid) REFERENCES vtiger_crmentity(crmid) ON DELETE CASCADE;CREATE TABLE vtiger_products (
    productid INTEGER(11) NOT NULL,
    product_no VARCHAR(100) NOT NULL,
    productname VARCHAR(50) NOT NULL,
    productcode VARCHAR(40),
    productcategory VARCHAR(200),
    manufacturer VARCHAR(200),
    qty_per_unit NUMERIC(11, 2) DEFAULT 0,
    unit_price NUMERIC(25, 2),
    weight NUMERIC(11, 3),
    pack_size INTEGER(11),
    sales_start_date DATE,
    sales_end_date DATE,
    start_date DATE,
    expiry_date DATE,
    cost_factor INTEGER(11),
    commissionrate NUMERIC(7, 3),
    commissionmethod VARCHAR(50),
    discontinued INTEGER(1) NOT NULL DEFAULT 0,
    usageunit VARCHAR(200),
    reorderlevel INTEGER(11),
    website VARCHAR(100),
    taxclass VARCHAR(200),
    mfr_part_no VARCHAR(200),
    vendor_part_no VARCHAR(200),
    serialno VARCHAR(200),
    qtyinstock NUMERIC(25, 3),
    productsheet VARCHAR(200),
    qtyindemand INTEGER(11),
    glacct VARCHAR(200),
    vendor_id INTEGER(11),
    imagename TEXT,
    currency_id INTEGER(19) NOT NULL DEFAULT 1,
    is_subproducts_viewable INTEGER(1) DEFAULT 1,
    PRIMARY KEY (productid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_products
ADD
  CONSTRAINT fk_1_vtiger_products FOREIGN KEY (productid) REFERENCES vtiger_crmentity(crmid) ON DELETE CASCADE;CREATE TABLE vtiger_currency (
    currencyid INTEGER(19) NOT NULL AUTO_INCREMENT,
    currency VARCHAR(200) NOT NULL,
    sortorderid INTEGER(19) NOT NULL DEFAULT 0,
    presence INTEGER(1) NOT NULL DEFAULT 1,
    PRIMARY KEY (currencyid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_currency
ADD
  UNIQUE INDEX currency_currency_idx (currency);CREATE TABLE vtiger_visibility (
    visibilityid INTEGER(19) NOT NULL AUTO_INCREMENT,
    visibility VARCHAR(200) NOT NULL,
    sortorderid INTEGER(19) NOT NULL DEFAULT 0,
    presence INTEGER(1) NOT NULL DEFAULT 1,
    PRIMARY KEY (visibilityid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_visibility
ADD
  UNIQUE INDEX visibility_visibility_idx (visibility);CREATE TABLE vtiger_manufacturer (
    manufacturerid INTEGER(19) NOT NULL AUTO_INCREMENT,
    manufacturer VARCHAR(200) NOT NULL,
    presence INTEGER(1) NOT NULL DEFAULT 1,
    picklist_valueid INTEGER(19) NOT NULL DEFAULT 0,
    sortorderid INTEGER(11),
    PRIMARY KEY (manufacturerid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_manufacturer
ADD
  UNIQUE INDEX manufacturer_manufacturer_idx (manufacturer);CREATE TABLE vtiger_role (
    roleid VARCHAR(255) NOT NULL,
    rolename VARCHAR(200),
    parentrole VARCHAR(255),
    depth INTEGER(19),
    PRIMARY KEY (roleid)
  ) Engine = InnoDB;CREATE TABLE vtiger_user2role (
    userid INTEGER(11) NOT NULL,
    roleid VARCHAR(255) NOT NULL,
    PRIMARY KEY (userid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_user2role
ADD
  INDEX user2role_roleid_idx (roleid);
ALTER TABLE
  vtiger_user2role
ADD
  CONSTRAINT fk_2_vtiger_user2role FOREIGN KEY (userid) REFERENCES vtiger_users(id) ON DELETE CASCADE;CREATE TABLE vtiger_groups (
    groupid INTEGER(19) NOT NULL,
    groupname VARCHAR(100),
    description TEXT,
    PRIMARY KEY (groupid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_groups
ADD
  UNIQUE INDEX groups_groupname_idx (groupname);CREATE TABLE vtiger_users2group (
    groupid INTEGER(19) NOT NULL,
    userid INTEGER(19) NOT NULL,
    PRIMARY KEY (groupid, userid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_users2group
ADD
  INDEX users2group_groupname_uerid_idx (groupid, userid);
ALTER TABLE
  vtiger_users2group
ADD
  CONSTRAINT fk_2_vtiger_users2group FOREIGN KEY (userid) REFERENCES vtiger_users(id) ON DELETE CASCADE;CREATE TABLE vtiger_group2grouprel (
    groupid INTEGER(19) NOT NULL,
    containsgroupid INTEGER(19) NOT NULL,
    PRIMARY KEY (groupid, containsgroupid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_group2grouprel
ADD
  CONSTRAINT fk_2_vtiger_group2grouprel FOREIGN KEY (groupid) REFERENCES vtiger_groups(groupid) ON DELETE CASCADE ON UPDATE CASCADE;CREATE TABLE vtiger_group2role (
    groupid INTEGER(19) NOT NULL,
    roleid VARCHAR(255) NOT NULL,
    PRIMARY KEY (groupid, roleid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_group2role
ADD
  CONSTRAINT fk_2_vtiger_group2role FOREIGN KEY (roleid) REFERENCES vtiger_role(roleid) ON DELETE CASCADE;CREATE TABLE vtiger_group2rs (
    groupid INTEGER(19) NOT NULL,
    roleandsubid VARCHAR(255) NOT NULL,
    PRIMARY KEY (groupid, roleandsubid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_group2rs
ADD
  CONSTRAINT fk_2_vtiger_group2rs FOREIGN KEY (roleandsubid) REFERENCES vtiger_role(roleid) ON DELETE CASCADE;CREATE TABLE vtiger_campaign (
    campaign_no VARCHAR(100) NOT NULL,
    campaignname VARCHAR(255),
    campaigntype VARCHAR(200),
    campaignstatus VARCHAR(200),
    expectedrevenue NUMERIC(25, 3),
    budgetcost NUMERIC(25, 3),
    actualcost NUMERIC(25, 3),
    expectedresponse VARCHAR(200),
    numsent NUMERIC(11, 0),
    product_id INTEGER(19),
    sponsor VARCHAR(255),
    targetaudience VARCHAR(255),
    targetsize INTEGER(19),
    expectedresponsecount INTEGER(19),
    expectedsalescount INTEGER(19),
    expectedroi NUMERIC(25, 3),
    actualresponsecount INTEGER(19),
    actualsalescount INTEGER(19),
    actualroi NUMERIC(25, 3),
    campaignid INTEGER(19) NOT NULL,
    closingdate DATE,
    PRIMARY KEY (campaignid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_campaign
ADD
  INDEX campaign_campaignstatus_idx (campaignstatus);
ALTER TABLE
  vtiger_campaign
ADD
  INDEX campaign_campaignname_idx (campaignname);
ALTER TABLE
  vtiger_campaign
ADD
  INDEX campaign_campaignid_idx (campaignid);CREATE TABLE vtiger_campaignscf (
    campaignid INTEGER(19) NOT NULL DEFAULT 0,
    PRIMARY KEY (campaignid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_campaignscf
ADD
  CONSTRAINT fk_1_vtiger_campaignscf FOREIGN KEY (campaignid) REFERENCES vtiger_campaign(campaignid) ON DELETE CASCADE;CREATE TABLE vtiger_campaigncontrel (
    campaignid INTEGER(19) NOT NULL DEFAULT 0,
    contactid INTEGER(19) NOT NULL DEFAULT 0,
    campaignrelstatusid INTEGER(19) NOT NULL DEFAULT 0,
    PRIMARY KEY (campaignid, contactid, campaignrelstatusid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_campaigncontrel
ADD
  INDEX campaigncontrel_contractid_idx (contactid);
ALTER TABLE
  vtiger_campaigncontrel
ADD
  CONSTRAINT fk_2_vtiger_campaigncontrel FOREIGN KEY (contactid) REFERENCES vtiger_contactdetails(contactid) ON DELETE CASCADE;CREATE TABLE vtiger_campaignleadrel (
    campaignid INTEGER(19) NOT NULL DEFAULT 0,
    leadid INTEGER(19) NOT NULL DEFAULT 0,
    campaignrelstatusid INTEGER(19) NOT NULL DEFAULT 0,
    PRIMARY KEY (campaignid, leadid, campaignrelstatusid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_campaignleadrel
ADD
  INDEX campaignleadrel_leadid_campaignid_idx (leadid, campaignid);
ALTER TABLE
  vtiger_campaignleadrel
ADD
  CONSTRAINT fk_2_vtiger_campaignleadrel FOREIGN KEY (leadid) REFERENCES vtiger_leaddetails(leadid) ON DELETE CASCADE;CREATE TABLE vtiger_contpotentialrel (
    contactid INTEGER(19) NOT NULL DEFAULT 0,
    potentialid INTEGER(19) NOT NULL DEFAULT 0,
    PRIMARY KEY (contactid, potentialid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_contpotentialrel
ADD
  INDEX contpotentialrel_potentialid_idx (potentialid);
ALTER TABLE
  vtiger_contpotentialrel
ADD
  INDEX contpotentialrel_contactid_idx (contactid);
ALTER TABLE
  vtiger_contpotentialrel
ADD
  CONSTRAINT fk_2_vtiger_contpotentialrel FOREIGN KEY (potentialid) REFERENCES vtiger_potential(potentialid) ON DELETE CASCADE;CREATE TABLE vtiger_cntactivityrel (
    contactid INTEGER(19) NOT NULL DEFAULT 0,
    activityid INTEGER(19) NOT NULL DEFAULT 0,
    PRIMARY KEY (contactid, activityid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_cntactivityrel
ADD
  INDEX cntactivityrel_contactid_idx (contactid);
ALTER TABLE
  vtiger_cntactivityrel
ADD
  INDEX cntactivityrel_activityid_idx (activityid);
ALTER TABLE
  vtiger_cntactivityrel
ADD
  CONSTRAINT fk_2_vtiger_cntactivityrel FOREIGN KEY (contactid) REFERENCES vtiger_contactdetails(contactid) ON DELETE CASCADE;CREATE TABLE vtiger_troubletickets (
    ticketid INTEGER(19) NOT NULL,
    ticket_no VARCHAR(100) NOT NULL,
    groupname VARCHAR(100),
    parent_id VARCHAR(100),
    product_id VARCHAR(100),
    priority VARCHAR(200),
    severity VARCHAR(200),
    status VARCHAR(200),
    category VARCHAR(200),
    title VARCHAR(255) NOT NULL,
    solution TEXT,
    update_log TEXT,
    version_id INTEGER(11),
    hours VARCHAR(200),
    days VARCHAR(200),
    PRIMARY KEY (ticketid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_troubletickets
ADD
  INDEX troubletickets_ticketid_idx (ticketid);
ALTER TABLE
  vtiger_troubletickets
ADD
  INDEX troubletickets_status_idx (status);
ALTER TABLE
  vtiger_troubletickets
ADD
  CONSTRAINT fk_1_vtiger_troubletickets FOREIGN KEY (ticketid) REFERENCES vtiger_crmentity(crmid) ON DELETE CASCADE;CREATE TABLE vtiger_ticketcomments (
    commentid INTEGER(19) NOT NULL AUTO_INCREMENT,
    ticketid INTEGER(19),
    comments TEXT,
    ownerid INTEGER(19) NOT NULL DEFAULT 0,
    ownertype VARCHAR(10),
    createdtime DATETIME NOT NULL,
    PRIMARY KEY (commentid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_ticketcomments
ADD
  INDEX ticketcomments_ticketid_idx (ticketid);
ALTER TABLE
  vtiger_ticketcomments
ADD
  CONSTRAINT fk_1_vtiger_ticketcomments FOREIGN KEY (ticketid) REFERENCES vtiger_troubletickets(ticketid) ON DELETE CASCADE;CREATE TABLE vtiger_salesmanactivityrel (
    smid INTEGER(19) NOT NULL DEFAULT 0,
    activityid INTEGER(19) NOT NULL DEFAULT 0,
    PRIMARY KEY (smid, activityid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_salesmanactivityrel
ADD
  INDEX salesmanactivityrel_activityid_idx (activityid);
ALTER TABLE
  vtiger_salesmanactivityrel
ADD
  INDEX salesmanactivityrel_smid_idx (smid);
ALTER TABLE
  vtiger_salesmanactivityrel
ADD
  CONSTRAINT fk_2_vtiger_salesmanactivityrel FOREIGN KEY (smid) REFERENCES vtiger_users(id) ON DELETE CASCADE;CREATE TABLE vtiger_vendorcontactrel (
    vendorid INTEGER(19) NOT NULL DEFAULT 0,
    contactid INTEGER(19) NOT NULL DEFAULT 0,
    PRIMARY KEY (vendorid, contactid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_vendorcontactrel
ADD
  INDEX vendorcontactrel_vendorid_idx (vendorid);
ALTER TABLE
  vtiger_vendorcontactrel
ADD
  INDEX vendorcontactrel_contact_idx (contactid);
ALTER TABLE
  vtiger_vendorcontactrel
ADD
  CONSTRAINT fk_2_vtiger_vendorcontactrel FOREIGN KEY (vendorid) REFERENCES vtiger_vendor(vendorid) ON DELETE CASCADE;CREATE TABLE vtiger_salesmanticketrel (
    smid INTEGER(19) NOT NULL DEFAULT 0,
    id INTEGER(19) NOT NULL DEFAULT 0,
    PRIMARY KEY (smid, id)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_salesmanticketrel
ADD
  INDEX salesmanticketrel_smid_idx (smid);
ALTER TABLE
  vtiger_salesmanticketrel
ADD
  INDEX salesmanticketrel_id_idx (id);
ALTER TABLE
  vtiger_salesmanticketrel
ADD
  CONSTRAINT fk_2_vtiger_salesmanticketrel FOREIGN KEY (smid) REFERENCES vtiger_users(id) ON DELETE CASCADE;CREATE TABLE vtiger_seactivityrel (
    crmid INTEGER(19) NOT NULL,
    activityid INTEGER(19) NOT NULL,
    PRIMARY KEY (crmid, activityid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_seactivityrel
ADD
  INDEX seactivityrel_activityid_idx (activityid);
ALTER TABLE
  vtiger_seactivityrel
ADD
  INDEX seactivityrel_crmid_idx (crmid);
ALTER TABLE
  vtiger_seactivityrel
ADD
  CONSTRAINT fk_2_vtiger_seactivityrel FOREIGN KEY (crmid) REFERENCES vtiger_crmentity(crmid) ON DELETE CASCADE;CREATE TABLE vtiger_seproductsrel (
    crmid INTEGER(19) NOT NULL DEFAULT 0,
    productid INTEGER(19) NOT NULL DEFAULT 0,
    setype VARCHAR(30) NOT NULL,
    quantity INTEGER(19) DEFAULT 1,
    PRIMARY KEY (crmid, productid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_seproductsrel
ADD
  INDEX seproductsrel_productid_idx (productid);
ALTER TABLE
  vtiger_seproductsrel
ADD
  INDEX seproductrel_crmid_idx (crmid);
ALTER TABLE
  vtiger_seproductsrel
ADD
  CONSTRAINT fk_2_vtiger_seproductsrel FOREIGN KEY (productid) REFERENCES vtiger_products(productid) ON DELETE CASCADE;CREATE TABLE vtiger_seticketsrel (
    crmid INTEGER(19) NOT NULL DEFAULT 0,
    ticketid INTEGER(19) NOT NULL DEFAULT 0,
    PRIMARY KEY (crmid, ticketid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_seticketsrel
ADD
  INDEX seticketsrel_crmid_idx (crmid);
ALTER TABLE
  vtiger_seticketsrel
ADD
  INDEX seticketsrel_ticketid_idx (ticketid);
ALTER TABLE
  vtiger_seticketsrel
ADD
  CONSTRAINT fk_2_vtiger_seticketsrel FOREIGN KEY (ticketid) REFERENCES vtiger_troubletickets(ticketid) ON DELETE CASCADE;CREATE TABLE vtiger_import_maps (
    id INTEGER(19) NOT NULL AUTO_INCREMENT,
    name VARCHAR(36) NOT NULL,
    module VARCHAR(36) NOT NULL,
    content LONGBLOB,
    has_header INTEGER(1) NOT NULL DEFAULT 1,
    deleted INTEGER(1) NOT NULL DEFAULT 0,
    date_entered TIMESTAMP NOT NULL,
    date_modified DATETIME,
    assigned_user_id VARCHAR(36),
    is_published VARCHAR(3) NOT NULL DEFAULT 'no',
    PRIMARY KEY (id)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_import_maps
ADD
  INDEX import_maps_assigned_user_id_module_name_deleted_idx (assigned_user_id, module, name, deleted);CREATE TABLE vtiger_systems (
    id INTEGER(19) NOT NULL,
    server VARCHAR(100),
    server_port INTEGER(19),
    server_username VARCHAR(100),
    server_password VARCHAR(100),
    server_type VARCHAR(20),
    smtp_auth VARCHAR(5),
    server_path VARCHAR(256),
    from_email_field VARCHAR(50),
    PRIMARY KEY (id)
  ) Engine = InnoDB;CREATE TABLE vtiger_currency_info (
    id INTEGER NOT NULL AUTO_INCREMENT,
    currency_name VARCHAR(100),
    currency_code VARCHAR(100),
    currency_symbol VARCHAR(30),
    conversion_rate NUMERIC(10, 3),
    currency_status VARCHAR(25),
    defaultid VARCHAR(10) NOT NULL DEFAULT '0',
    deleted INTEGER(1) NOT NULL DEFAULT 0,
    PRIMARY KEY (id)
  ) Engine = InnoDB;CREATE TABLE vtiger_defaultcv (
    tabid INTEGER(19) NOT NULL,
    defaultviewname VARCHAR(50) NOT NULL,
    query TEXT,
    PRIMARY KEY (tabid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_defaultcv
ADD
  CONSTRAINT fk_1_vtiger_defaultcv FOREIGN KEY (tabid) REFERENCES vtiger_tab(tabid) ON DELETE CASCADE;CREATE TABLE vtiger_emailtemplates (
    foldername VARCHAR(100),
    templatename VARCHAR(100),
    templatepath VARCHAR(100),
    subject VARCHAR(100),
    description TEXT,
    body TEXT,
    deleted INTEGER(1) NOT NULL DEFAULT 0,
    templateid INTEGER(19) NOT NULL AUTO_INCREMENT,
    systemtemplate INTEGER(1) NOT NULL DEFAULT 0,
    module VARCHAR(100),
    PRIMARY KEY (templateid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_emailtemplates
ADD
  INDEX emailtemplates_foldernamd_templatename_subject_idx (foldername, templatename, subject);CREATE TABLE vtiger_faq (
    id INTEGER(11) NOT NULL AUTO_INCREMENT,
    faq_no VARCHAR(100) NOT NULL,
    product_id VARCHAR(100),
    question TEXT,
    answer TEXT,
    category VARCHAR(200) NOT NULL,
    status VARCHAR(200) NOT NULL,
    PRIMARY KEY (id)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_faq
ADD
  INDEX faq_id_idx (id);
ALTER TABLE
  vtiger_faq
ADD
  CONSTRAINT fk_1_vtiger_faq FOREIGN KEY (id) REFERENCES vtiger_crmentity(crmid) ON DELETE CASCADE;CREATE TABLE vtiger_faqcomments (
    commentid INTEGER(19) NOT NULL AUTO_INCREMENT,
    faqid INTEGER(19),
    comments TEXT,
    createdtime DATETIME NOT NULL,
    PRIMARY KEY (commentid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_faqcomments
ADD
  INDEX faqcomments_faqid_idx (faqid);
ALTER TABLE
  vtiger_faqcomments
ADD
  CONSTRAINT fk_1_vtiger_faqcomments FOREIGN KEY (faqid) REFERENCES vtiger_faq(id) ON DELETE CASCADE;CREATE TABLE vtiger_loginhistory (
    login_id INTEGER(11) NOT NULL AUTO_INCREMENT,
    user_name VARCHAR(25) NOT NULL,
    user_ip VARCHAR(25) NOT NULL,
    logout_time TIMESTAMP,
    login_time DATETIME,
    status VARCHAR(25),
    PRIMARY KEY (login_id)
  ) Engine = InnoDB;CREATE TABLE vtiger_senotesrel (
    crmid INTEGER(19) NOT NULL DEFAULT 0,
    notesid INTEGER(19) NOT NULL DEFAULT 0,
    PRIMARY KEY (crmid, notesid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_senotesrel
ADD
  INDEX senotesrel_notesid_idx (notesid);
ALTER TABLE
  vtiger_senotesrel
ADD
  INDEX senotesrel_crmid_idx (crmid);
ALTER TABLE
  vtiger_senotesrel
ADD
  CONSTRAINT fk_2_vtiger_senotesrel FOREIGN KEY (notesid) REFERENCES vtiger_notes(notesid) ON DELETE CASCADE;CREATE TABLE vtiger_ticketcf (
    ticketid INTEGER(19) NOT NULL DEFAULT 0,
    from_portal VARCHAR(3),
    PRIMARY KEY (ticketid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_ticketcf
ADD
  CONSTRAINT fk_1_vtiger_ticketcf FOREIGN KEY (ticketid) REFERENCES vtiger_troubletickets(ticketid) ON DELETE CASCADE;CREATE TABLE vtiger_productcf (
    productid INTEGER(19) NOT NULL DEFAULT 0,
    PRIMARY KEY (productid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_productcf
ADD
  CONSTRAINT fk_1_vtiger_productcf FOREIGN KEY (productid) REFERENCES vtiger_products(productid) ON DELETE CASCADE;CREATE TABLE vtiger_users_last_import (
    id INTEGER(36) NOT NULL AUTO_INCREMENT,
    assigned_user_id VARCHAR(36),
    bean_type VARCHAR(36),
    bean_id VARCHAR(36),
    deleted INTEGER(1) NOT NULL DEFAULT 0,
    PRIMARY KEY (id)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_users_last_import
ADD
  INDEX idx_user_id (assigned_user_id);CREATE TABLE vtiger_wordtemplates (
    templateid INTEGER(19) NOT NULL,
    filename VARCHAR(100) NOT NULL,
    module VARCHAR(30) NOT NULL,
    date_entered TIMESTAMP NOT NULL,
    parent_type VARCHAR(50) NOT NULL,
    data LONGBLOB,
    description TEXT,
    filesize VARCHAR(50) NOT NULL,
    filetype VARCHAR(20) NOT NULL,
    deleted INTEGER(1) NOT NULL DEFAULT 0,
    PRIMARY KEY (templateid)
  ) Engine = InnoDB;CREATE TABLE vtiger_accountrating (
    accountratingid INTEGER(19) NOT NULL AUTO_INCREMENT,
    rating VARCHAR(200) NOT NULL,
    sortorderid INTEGER(19) NOT NULL DEFAULT 0,
    presence INTEGER(1) NOT NULL DEFAULT 1,
    PRIMARY KEY (accountratingid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_accountrating
ADD
  UNIQUE INDEX accountrating_rating_idx (rating);CREATE TABLE vtiger_accounttype (
    accounttypeid INTEGER(19) NOT NULL AUTO_INCREMENT,
    accounttype VARCHAR(200) NOT NULL,
    presence INTEGER(1) NOT NULL DEFAULT 1,
    picklist_valueid INTEGER(19) NOT NULL DEFAULT 0,
    sortorderid INTEGER(11),
    PRIMARY KEY (accounttypeid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_accounttype
ADD
  UNIQUE INDEX accounttype_accounttype_idx (accounttype);CREATE TABLE vtiger_leadsource (
    leadsourceid INTEGER(19) NOT NULL AUTO_INCREMENT,
    leadsource VARCHAR(200) NOT NULL,
    presence INTEGER(1) NOT NULL DEFAULT 1,
    picklist_valueid INTEGER(19) NOT NULL DEFAULT 0,
    sortorderid INTEGER(11),
    PRIMARY KEY (leadsourceid)
  ) Engine = InnoDB;CREATE TABLE vtiger_opportunity_type (
    opptypeid INTEGER(19) NOT NULL AUTO_INCREMENT,
    opportunity_type VARCHAR(200) NOT NULL,
    presence INTEGER(1) NOT NULL DEFAULT 1,
    picklist_valueid INTEGER(19) NOT NULL DEFAULT 0,
    sortorderid INTEGER(11),
    PRIMARY KEY (opptypeid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_opportunity_type
ADD
  UNIQUE INDEX opportunity_type_opportunity_type_idx (opportunity_type);CREATE TABLE vtiger_leadstage (
    leadstageid INTEGER(19) NOT NULL AUTO_INCREMENT,
    stage VARCHAR(200) NOT NULL,
    sortorderid INTEGER(19) NOT NULL DEFAULT 0,
    presence INTEGER(1) NOT NULL DEFAULT 1,
    PRIMARY KEY (leadstageid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_leadstage
ADD
  UNIQUE INDEX leadstage_stage_idx (stage);CREATE TABLE vtiger_leadstatus (
    leadstatusid INTEGER(19) NOT NULL AUTO_INCREMENT,
    leadstatus VARCHAR(200) NOT NULL,
    presence INTEGER(1) NOT NULL DEFAULT 1,
    picklist_valueid INTEGER(19) NOT NULL DEFAULT 0,
    sortorderid INTEGER(11),
    PRIMARY KEY (leadstatusid)
  ) Engine = InnoDB;CREATE TABLE vtiger_eventstatus (
    eventstatusid INTEGER(19) NOT NULL AUTO_INCREMENT,
    eventstatus VARCHAR(200) NOT NULL,
    presence INTEGER(1) NOT NULL DEFAULT 1,
    picklist_valueid INTEGER(19) NOT NULL DEFAULT 0,
    sortorderid INTEGER(11),
    PRIMARY KEY (eventstatusid)
  ) Engine = InnoDB;CREATE TABLE vtiger_duration_minutes (
    minutesid INTEGER(19) NOT NULL AUTO_INCREMENT,
    duration_minutes VARCHAR(200) NOT NULL,
    sortorderid INTEGER(19) NOT NULL DEFAULT 0,
    presence INTEGER(1) NOT NULL DEFAULT 1,
    PRIMARY KEY (minutesid)
  ) Engine = InnoDB;CREATE TABLE vtiger_opportunitystage (
    potstageid INTEGER(19) NOT NULL AUTO_INCREMENT,
    stage VARCHAR(200) NOT NULL,
    sortorderid INTEGER(19) NOT NULL DEFAULT 0,
    presence INTEGER(1) NOT NULL DEFAULT 1,
    probability NUMERIC(3, 2) DEFAULT 0,
    PRIMARY KEY (potstageid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_opportunitystage
ADD
  UNIQUE INDEX opportunitystage_stage_idx (stage);CREATE TABLE vtiger_priority (
    priorityid INTEGER(19) NOT NULL AUTO_INCREMENT,
    priority VARCHAR(200) NOT NULL,
    sortorderid INTEGER(19) NOT NULL DEFAULT 0,
    presence INTEGER(1) NOT NULL DEFAULT 1,
    PRIMARY KEY (priorityid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_priority
ADD
  UNIQUE INDEX priority_priority_idx (priority);CREATE TABLE vtiger_industry (
    industryid INTEGER(19) NOT NULL AUTO_INCREMENT,
    industry VARCHAR(200) NOT NULL,
    presence INTEGER(1) NOT NULL DEFAULT 1,
    picklist_valueid INTEGER(19) NOT NULL DEFAULT 0,
    sortorderid INTEGER(11),
    PRIMARY KEY (industryid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_industry
ADD
  UNIQUE INDEX industry_industry_idx (industry);CREATE TABLE vtiger_salutationtype (
    salutationid INTEGER(19) NOT NULL AUTO_INCREMENT,
    salutationtype VARCHAR(200) NOT NULL,
    presence INTEGER(1) NOT NULL DEFAULT 1,
    picklist_valueid INTEGER(19) NOT NULL DEFAULT 0,
    sortorderid INTEGER(11),
    PRIMARY KEY (salutationid)
  ) Engine = InnoDB;CREATE TABLE vtiger_taskpriority (
    taskpriorityid INTEGER(19) NOT NULL AUTO_INCREMENT,
    taskpriority VARCHAR(200),
    presence INTEGER(1) NOT NULL DEFAULT 1,
    picklist_valueid INTEGER(19) NOT NULL DEFAULT 0,
    sortorderid INTEGER(11),
    PRIMARY KEY (taskpriorityid)
  ) Engine = InnoDB;CREATE TABLE vtiger_taskstatus (
    taskstatusid INTEGER(19) NOT NULL AUTO_INCREMENT,
    taskstatus VARCHAR(200),
    presence INTEGER(1) NOT NULL DEFAULT 1,
    picklist_valueid INTEGER(19) NOT NULL DEFAULT 0,
    sortorderid INTEGER(11),
    PRIMARY KEY (taskstatusid)
  ) Engine = InnoDB;CREATE TABLE vtiger_activitytype (
    activitytypeid INTEGER(19) NOT NULL AUTO_INCREMENT,
    activitytype VARCHAR(200) NOT NULL,
    presence INTEGER(1) NOT NULL DEFAULT 1,
    picklist_valueid INTEGER(19) NOT NULL DEFAULT 0,
    sortorderid INTEGER(11),
    PRIMARY KEY (activitytypeid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_activitytype
ADD
  UNIQUE INDEX activitytype_activitytype_idx (activitytype);CREATE TABLE vtiger_sales_stage (
    sales_stage_id INTEGER(19) NOT NULL AUTO_INCREMENT,
    sales_stage VARCHAR(200),
    presence INTEGER(1) NOT NULL DEFAULT 1,
    picklist_valueid INTEGER(19) NOT NULL DEFAULT 0,
    sortorderid INTEGER(11),
    PRIMARY KEY (sales_stage_id)
  ) Engine = InnoDB;CREATE TABLE vtiger_faqcategories (
    faqcategories_id INTEGER(19) NOT NULL AUTO_INCREMENT,
    faqcategories VARCHAR(200),
    presence INTEGER(1) NOT NULL DEFAULT 1,
    picklist_valueid INTEGER(19) NOT NULL DEFAULT 0,
    sortorderid INTEGER(11),
    PRIMARY KEY (faqcategories_id)
  ) Engine = InnoDB;CREATE TABLE vtiger_faqstatus (
    faqstatus_id INTEGER(19) NOT NULL AUTO_INCREMENT,
    faqstatus VARCHAR(200),
    presence INTEGER(1) NOT NULL DEFAULT 1,
    picklist_valueid INTEGER(19) NOT NULL DEFAULT 0,
    sortorderid INTEGER(11),
    PRIMARY KEY (faqstatus_id)
  ) Engine = InnoDB;CREATE TABLE vtiger_faqcf (
    faqid INTEGER(19) NOT NULL DEFAULT 0,
    PRIMARY KEY (faqid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_faqcf
ADD
  CONSTRAINT fk_1_vtiger_faqcf FOREIGN KEY (faqid) REFERENCES vtiger_faq(id) ON DELETE CASCADE;CREATE TABLE vtiger_rating (
    rating_id INTEGER(19) NOT NULL AUTO_INCREMENT,
    rating VARCHAR(200),
    presence INTEGER(1) NOT NULL DEFAULT 1,
    picklist_valueid INTEGER(19) NOT NULL DEFAULT 0,
    sortorderid INTEGER(11),
    PRIMARY KEY (rating_id)
  ) Engine = InnoDB;CREATE TABLE vtiger_ticketcategories (
    ticketcategories_id INTEGER(19) NOT NULL AUTO_INCREMENT,
    ticketcategories VARCHAR(200),
    presence INTEGER(1) NOT NULL DEFAULT 0,
    picklist_valueid INTEGER(19) NOT NULL DEFAULT 0,
    sortorderid INTEGER(11),
    PRIMARY KEY (ticketcategories_id)
  ) Engine = InnoDB;CREATE TABLE vtiger_ticketpriorities (
    ticketpriorities_id INTEGER(19) NOT NULL AUTO_INCREMENT,
    ticketpriorities VARCHAR(200),
    presence INTEGER(1) NOT NULL DEFAULT 0,
    picklist_valueid INTEGER(19) NOT NULL DEFAULT 0,
    sortorderid INTEGER(11),
    PRIMARY KEY (ticketpriorities_id)
  ) Engine = InnoDB;CREATE TABLE vtiger_ticketseverities (
    ticketseverities_id INTEGER(19) NOT NULL AUTO_INCREMENT,
    ticketseverities VARCHAR(200),
    presence INTEGER(1) NOT NULL DEFAULT 0,
    picklist_valueid INTEGER(19) NOT NULL DEFAULT 0,
    sortorderid INTEGER(11),
    PRIMARY KEY (ticketseverities_id)
  ) Engine = InnoDB;CREATE TABLE vtiger_ticketstatus (
    ticketstatus_id INTEGER(19) NOT NULL AUTO_INCREMENT,
    ticketstatus VARCHAR(200),
    presence INTEGER(1) NOT NULL DEFAULT 0,
    picklist_valueid INTEGER(19) NOT NULL DEFAULT 0,
    sortorderid INTEGER(11),
    PRIMARY KEY (ticketstatus_id)
  ) Engine = InnoDB;CREATE TABLE vtiger_durationhrs (
    hrsid INTEGER(19) NOT NULL AUTO_INCREMENT,
    hrs VARCHAR(50),
    sortorderid INTEGER(19) NOT NULL DEFAULT 0,
    presence INTEGER(1) NOT NULL DEFAULT 1,
    PRIMARY KEY (hrsid)
  ) Engine = InnoDB;CREATE TABLE vtiger_durationmins (
    minsid INTEGER(19) NOT NULL AUTO_INCREMENT,
    mins VARCHAR(50),
    sortorderid INTEGER(19) NOT NULL DEFAULT 0,
    presence INTEGER(1) NOT NULL DEFAULT 1,
    PRIMARY KEY (minsid)
  ) Engine = InnoDB;CREATE TABLE vtiger_profile (
    profileid INTEGER(10) NOT NULL AUTO_INCREMENT,
    profilename VARCHAR(50) NOT NULL,
    description TEXT,
    PRIMARY KEY (profileid)
  ) Engine = InnoDB;CREATE TABLE vtiger_profile2globalpermissions (
    profileid INTEGER(19) NOT NULL,
    globalactionid INTEGER(19) NOT NULL,
    globalactionpermission INTEGER(19),
    PRIMARY KEY (profileid, globalactionid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_profile2globalpermissions
ADD
  INDEX idx_profile2globalpermissions (profileid, globalactionid);
ALTER TABLE
  vtiger_profile2globalpermissions
ADD
  CONSTRAINT fk_1_vtiger_profile2globalpermissions FOREIGN KEY (profileid) REFERENCES vtiger_profile(profileid) ON DELETE CASCADE;CREATE TABLE vtiger_profile2tab (
    profileid INTEGER(11),
    tabid INTEGER(10),
    permissions INTEGER(10) NOT NULL DEFAULT 0
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_profile2tab
ADD
  INDEX profile2tab_profileid_tabid_idx (profileid, tabid);CREATE TABLE vtiger_profile2standardpermissions (
    profileid INTEGER(11) NOT NULL,
    tabid INTEGER(10) NOT NULL,
    operation INTEGER(10) NOT NULL,
    permissions INTEGER(1),
    PRIMARY KEY (profileid, tabid, operation)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_profile2standardpermissions
ADD
  INDEX profile2standardpermissions_profileid_tabid_operation_idx (profileid, tabid, operation);CREATE TABLE vtiger_profile2field (
    profileid INTEGER(11) NOT NULL,
    tabid INTEGER(10),
    fieldid INTEGER(19) NOT NULL,
    visible INTEGER(19),
    readonly INTEGER(19),
    PRIMARY KEY (profileid, fieldid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_profile2field
ADD
  INDEX profile2field_profileid_tabid_fieldname_idx (profileid, tabid);
ALTER TABLE
  vtiger_profile2field
ADD
  INDEX profile2field_tabid_profileid_idx (tabid, profileid);
ALTER TABLE
  vtiger_profile2field
ADD
  INDEX profile2field_visible_profileid_idx (visible, profileid);CREATE TABLE vtiger_role2profile (
    roleid VARCHAR(255) NOT NULL,
    profileid INTEGER(11) NOT NULL,
    PRIMARY KEY (roleid, profileid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_role2profile
ADD
  INDEX role2profile_roleid_profileid_idx (roleid, profileid);CREATE TABLE vtiger_org_share_action_mapping (
    share_action_id INTEGER(19) NOT NULL,
    share_action_name VARCHAR(200),
    PRIMARY KEY (share_action_id)
  ) Engine = InnoDB;CREATE TABLE vtiger_def_org_share (
    ruleid INTEGER(11) NOT NULL AUTO_INCREMENT,
    tabid INTEGER(11) NOT NULL,
    permission INTEGER(19),
    editstatus INTEGER(19),
    PRIMARY KEY (ruleid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_def_org_share
ADD
  CONSTRAINT fk_1_vtiger_def_org_share FOREIGN KEY (permission) REFERENCES vtiger_org_share_action_mapping(share_action_id) ON DELETE CASCADE;CREATE TABLE vtiger_def_org_field (
    tabid INTEGER(10),
    fieldid INTEGER(19) NOT NULL,
    visible INTEGER(19),
    readonly INTEGER(19),
    PRIMARY KEY (fieldid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_def_org_field
ADD
  INDEX def_org_field_tabid_fieldid_idx (tabid, fieldid);
ALTER TABLE
  vtiger_def_org_field
ADD
  INDEX def_org_field_tabid_idx (tabid);
ALTER TABLE
  vtiger_def_org_field
ADD
  INDEX def_org_field_visible_fieldid_idx (visible, fieldid);CREATE TABLE vtiger_profile2utility (
    profileid INTEGER(11) NOT NULL,
    tabid INTEGER(11) NOT NULL,
    activityid INTEGER(11) NOT NULL,
    permission INTEGER(1),
    PRIMARY KEY (profileid, tabid, activityid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_profile2utility
ADD
  INDEX profile2utility_profileid_tabid_activityid_idx (profileid, tabid, activityid);CREATE TABLE vtiger_productcategory (
    productcategoryid INTEGER(19) NOT NULL AUTO_INCREMENT,
    productcategory VARCHAR(200) NOT NULL,
    presence INTEGER(1) NOT NULL DEFAULT 1,
    picklist_valueid INTEGER(19) NOT NULL DEFAULT 0,
    sortorderid INTEGER(11),
    PRIMARY KEY (productcategoryid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_productcategory
ADD
  UNIQUE INDEX productcategory_productcategory_idx (productcategory);CREATE TABLE vtiger_salesorder (
    salesorderid INTEGER(19) NOT NULL DEFAULT 0,
    subject VARCHAR(100),
    potentialid INTEGER(19),
    customerno VARCHAR(100),
    salesorder_no VARCHAR(100),
    quoteid INTEGER(19),
    vendorterms VARCHAR(100),
    contactid INTEGER(19),
    vendorid INTEGER(19),
    duedate DATE,
    carrier VARCHAR(200),
    pending VARCHAR(200),
    type VARCHAR(100),
    adjustment NUMERIC(25, 3),
    salescommission NUMERIC(25, 3),
    exciseduty NUMERIC(25, 3),
    total NUMERIC(25, 3),
    subtotal NUMERIC(25, 3),
    taxtype VARCHAR(25),
    discount_percent NUMERIC(25, 3),
    discount_amount NUMERIC(25, 3),
    s_h_amount NUMERIC(25, 3),
    accountid INTEGER(19),
    terms_conditions TEXT,
    purchaseorder VARCHAR(200),
    sostatus VARCHAR(200),
    currency_id INTEGER(19) NOT NULL DEFAULT 1,
    conversion_rate NUMERIC(10, 3) NOT NULL DEFAULT 1.000,
    enable_recurring INTEGER DEFAULT 0,
    compound_taxes_info TEXT,
    PRIMARY KEY (salesorderid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_salesorder
ADD
  INDEX salesorder_vendorid_idx (vendorid);
ALTER TABLE
  vtiger_salesorder
ADD
  INDEX salesorder_contactid_idx (contactid);
ALTER TABLE
  vtiger_salesorder
ADD
  CONSTRAINT fk_3_vtiger_salesorder FOREIGN KEY (vendorid) REFERENCES vtiger_vendor(vendorid) ON DELETE CASCADE;CREATE TABLE vtiger_sobillads (
    sobilladdressid INTEGER(19) NOT NULL DEFAULT 0,
    bill_city VARCHAR(30),
    bill_code VARCHAR(30),
    bill_country VARCHAR(30),
    bill_state VARCHAR(30),
    bill_street VARCHAR(250),
    bill_pobox VARCHAR(30),
    PRIMARY KEY (sobilladdressid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_sobillads
ADD
  CONSTRAINT fk_1_vtiger_sobillads FOREIGN KEY (sobilladdressid) REFERENCES vtiger_salesorder(salesorderid) ON DELETE CASCADE;CREATE TABLE vtiger_soshipads (
    soshipaddressid INTEGER(19) NOT NULL DEFAULT 0,
    ship_city VARCHAR(30),
    ship_code VARCHAR(30),
    ship_country VARCHAR(30),
    ship_state VARCHAR(30),
    ship_street VARCHAR(250),
    ship_pobox VARCHAR(30),
    PRIMARY KEY (soshipaddressid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_soshipads
ADD
  CONSTRAINT fk_1_vtiger_soshipads FOREIGN KEY (soshipaddressid) REFERENCES vtiger_salesorder(salesorderid) ON DELETE CASCADE;CREATE TABLE vtiger_salesordercf (
    salesorderid INTEGER(19) NOT NULL DEFAULT 0,
    PRIMARY KEY (salesorderid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_salesordercf
ADD
  CONSTRAINT fk_1_vtiger_salesordercf FOREIGN KEY (salesorderid) REFERENCES vtiger_salesorder(salesorderid) ON DELETE CASCADE;CREATE TABLE vtiger_notificationscheduler (
    schedulednotificationid INTEGER(19) NOT NULL AUTO_INCREMENT,
    schedulednotificationname VARCHAR(200),
    active INTEGER(1),
    notificationsubject VARCHAR(200),
    notificationbody TEXT,
    label VARCHAR(50),
    type VARCHAR(10),
    PRIMARY KEY (schedulednotificationid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_notificationscheduler
ADD
  UNIQUE INDEX notificationscheduler_schedulednotificationname_idx (schedulednotificationname);CREATE TABLE vtiger_activityproductrel (
    activityid INTEGER(19) NOT NULL DEFAULT 0,
    productid INTEGER(19) NOT NULL DEFAULT 0,
    PRIMARY KEY (activityid, productid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_activityproductrel
ADD
  INDEX activityproductrel_activityid_idx (activityid);
ALTER TABLE
  vtiger_activityproductrel
ADD
  INDEX activityproductrel_productid_idx (productid);
ALTER TABLE
  vtiger_activityproductrel
ADD
  CONSTRAINT fk_2_vtiger_activityproductrel FOREIGN KEY (productid) REFERENCES vtiger_products(productid) ON DELETE CASCADE;CREATE TABLE vtiger_relatedlists (
    relation_id INTEGER(19) NOT NULL,
    tabid INTEGER(10),
    related_tabid INTEGER(10),
    name VARCHAR(100),
    sequence INTEGER(10),
    label VARCHAR(100),
    presence INTEGER(10) NOT NULL DEFAULT 0,
    actions VARCHAR(50) NOT NULL DEFAULT '',
    relationfieldid INTEGER(19),
    source VARCHAR(25),
    relationtype VARCHAR(10),
    PRIMARY KEY (relation_id)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_relatedlists
ADD
  INDEX relatedlists_relation_id_idx (relation_id);CREATE TABLE vtiger_rss (
    rssid INTEGER(19) NOT NULL,
    rssurl VARCHAR(200) NOT NULL DEFAULT '',
    rsstitle VARCHAR(200),
    rsstype INTEGER(10) DEFAULT 0,
    starred INTEGER(1) DEFAULT 0,
    PRIMARY KEY (rssid)
  ) Engine = InnoDB;CREATE TABLE vtiger_vendorcf (
    vendorid INTEGER(19) NOT NULL DEFAULT 0,
    PRIMARY KEY (vendorid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_vendorcf
ADD
  CONSTRAINT fk_1_vtiger_vendorcf FOREIGN KEY (vendorid) REFERENCES vtiger_vendor(vendorid) ON DELETE CASCADE;CREATE TABLE vtiger_pricebook (
    pricebookid INTEGER(19) NOT NULL DEFAULT 0,
    pricebook_no VARCHAR(100) NOT NULL,
    bookname VARCHAR(100),
    active INTEGER(1),
    currency_id INTEGER(19) NOT NULL DEFAULT 1,
    PRIMARY KEY (pricebookid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_pricebook
ADD
  CONSTRAINT fk_1_vtiger_pricebook FOREIGN KEY (pricebookid) REFERENCES vtiger_crmentity(crmid) ON DELETE CASCADE;CREATE TABLE vtiger_pricebookcf (
    pricebookid INTEGER(19) NOT NULL DEFAULT 0,
    PRIMARY KEY (pricebookid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_pricebookcf
ADD
  CONSTRAINT fk_1_vtiger_pricebookcf FOREIGN KEY (pricebookid) REFERENCES vtiger_pricebook(pricebookid) ON DELETE CASCADE;CREATE TABLE vtiger_pricebookproductrel (
    pricebookid INTEGER(19) NOT NULL,
    productid INTEGER(19) NOT NULL,
    listprice NUMERIC(25, 3),
    usedcurrency INTEGER(11) NOT NULL DEFAULT 1,
    PRIMARY KEY (pricebookid, productid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_pricebookproductrel
ADD
  INDEX pricebookproductrel_pricebookid_idx (pricebookid);
ALTER TABLE
  vtiger_pricebookproductrel
ADD
  INDEX pricebookproductrel_productid_idx (productid);
ALTER TABLE
  vtiger_pricebookproductrel
ADD
  CONSTRAINT fk_1_vtiger_pricebookproductrel FOREIGN KEY (pricebookid) REFERENCES vtiger_pricebook(pricebookid) ON DELETE CASCADE;CREATE TABLE vtiger_mail_accounts (
    account_id INTEGER(11) NOT NULL,
    user_id INTEGER(11) NOT NULL,
    display_name VARCHAR(50),
    mail_id VARCHAR(50),
    account_name VARCHAR(50),
    mail_protocol VARCHAR(20),
    mail_username VARCHAR(50) NOT NULL,
    mail_password VARCHAR(250) NOT NULL,
    mail_servername VARCHAR(50),
    box_refresh INTEGER(10),
    mails_per_page INTEGER(10),
    ssltype VARCHAR(50),
    sslmeth VARCHAR(50),
    int_mailer INTEGER(1) DEFAULT 0,
    status VARCHAR(10),
    set_default INTEGER(2),
    PRIMARY KEY (account_id)
  ) Engine = InnoDB;CREATE TABLE vtiger_quotes (
    quoteid INTEGER(19) NOT NULL DEFAULT 0,
    subject VARCHAR(100),
    potentialid INTEGER(19),
    quotestage VARCHAR(200),
    validtill DATE,
    contactid INTEGER(19),
    quote_no VARCHAR(100),
    subtotal NUMERIC(25, 3),
    carrier VARCHAR(200),
    shipping VARCHAR(100),
    inventorymanager INTEGER(19),
    type VARCHAR(100),
    adjustment NUMERIC(25, 3),
    total NUMERIC(25, 3),
    taxtype VARCHAR(25),
    discount_percent NUMERIC(25, 3),
    discount_amount NUMERIC(25, 3),
    s_h_amount NUMERIC(25, 3),
    accountid INTEGER(19),
    terms_conditions TEXT,
    currency_id INTEGER(19) NOT NULL DEFAULT 1,
    conversion_rate NUMERIC(10, 3) NOT NULL DEFAULT 1.000,
    compound_taxes_info TEXT,
    PRIMARY KEY (quoteid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_quotes
ADD
  INDEX quote_quotestage_idx (quotestage);
ALTER TABLE
  vtiger_quotes
ADD
  INDEX quotes_potentialid_idx (potentialid);
ALTER TABLE
  vtiger_quotes
ADD
  INDEX quotes_contactid_idx (contactid);
ALTER TABLE
  vtiger_quotes
ADD
  CONSTRAINT fk_3_vtiger_quotes FOREIGN KEY (potentialid) REFERENCES vtiger_potential(potentialid) ON DELETE CASCADE;CREATE TABLE vtiger_quotesbillads (
    quotebilladdressid INTEGER(19) NOT NULL DEFAULT 0,
    bill_city VARCHAR(30),
    bill_code VARCHAR(30),
    bill_country VARCHAR(30),
    bill_state VARCHAR(30),
    bill_street VARCHAR(250),
    bill_pobox VARCHAR(30),
    PRIMARY KEY (quotebilladdressid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_quotesbillads
ADD
  CONSTRAINT fk_1_vtiger_quotesbillads FOREIGN KEY (quotebilladdressid) REFERENCES vtiger_quotes(quoteid) ON DELETE CASCADE;CREATE TABLE vtiger_quotesshipads (
    quoteshipaddressid INTEGER(19) NOT NULL DEFAULT 0,
    ship_city VARCHAR(30),
    ship_code VARCHAR(30),
    ship_country VARCHAR(30),
    ship_state VARCHAR(30),
    ship_street VARCHAR(250),
    ship_pobox VARCHAR(30),
    PRIMARY KEY (quoteshipaddressid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_quotesshipads
ADD
  CONSTRAINT fk_1_vtiger_quotesshipads FOREIGN KEY (quoteshipaddressid) REFERENCES vtiger_quotes(quoteid) ON DELETE CASCADE;CREATE TABLE vtiger_quotescf (
    quoteid INTEGER(19) NOT NULL DEFAULT 0,
    PRIMARY KEY (quoteid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_quotescf
ADD
  CONSTRAINT fk_1_vtiger_quotescf FOREIGN KEY (quoteid) REFERENCES vtiger_quotes(quoteid) ON DELETE CASCADE;CREATE TABLE vtiger_purchaseorder (
    purchaseorderid INTEGER(19) NOT NULL DEFAULT 0,
    subject VARCHAR(100),
    quoteid INTEGER(19),
    vendorid INTEGER(19),
    requisition_no VARCHAR(100),
    purchaseorder_no VARCHAR(100),
    tracking_no VARCHAR(100),
    contactid INTEGER(19),
    duedate DATE,
    carrier VARCHAR(200),
    type VARCHAR(100),
    adjustment NUMERIC(25, 3),
    salescommission NUMERIC(25, 3),
    exciseduty NUMERIC(25, 3),
    total NUMERIC(25, 3),
    subtotal NUMERIC(25, 3),
    taxtype VARCHAR(25),
    discount_percent NUMERIC(25, 3),
    discount_amount NUMERIC(25, 3),
    s_h_amount NUMERIC(25, 3),
    terms_conditions TEXT,
    postatus VARCHAR(200),
    currency_id INTEGER(19) NOT NULL DEFAULT 1,
    conversion_rate NUMERIC(10, 3) NOT NULL DEFAULT 1.000,
    compound_taxes_info TEXT,
    PRIMARY KEY (purchaseorderid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_purchaseorder
ADD
  INDEX purchaseorder_vendorid_idx (vendorid);
ALTER TABLE
  vtiger_purchaseorder
ADD
  INDEX purchaseorder_quoteid_idx (quoteid);
ALTER TABLE
  vtiger_purchaseorder
ADD
  INDEX purchaseorder_contactid_idx (contactid);
ALTER TABLE
  vtiger_purchaseorder
ADD
  CONSTRAINT fk_4_vtiger_purchaseorder FOREIGN KEY (vendorid) REFERENCES vtiger_vendor(vendorid) ON DELETE CASCADE;CREATE TABLE vtiger_pobillads (
    pobilladdressid INTEGER(19) NOT NULL DEFAULT 0,
    bill_city VARCHAR(30),
    bill_code VARCHAR(30),
    bill_country VARCHAR(30),
    bill_state VARCHAR(30),
    bill_street VARCHAR(250),
    bill_pobox VARCHAR(30),
    PRIMARY KEY (pobilladdressid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_pobillads
ADD
  CONSTRAINT fk_1_vtiger_pobillads FOREIGN KEY (pobilladdressid) REFERENCES vtiger_purchaseorder(purchaseorderid) ON DELETE CASCADE;CREATE TABLE vtiger_poshipads (
    poshipaddressid INTEGER(19) NOT NULL DEFAULT 0,
    ship_city VARCHAR(30),
    ship_code VARCHAR(30),
    ship_country VARCHAR(30),
    ship_state VARCHAR(30),
    ship_street VARCHAR(250),
    ship_pobox VARCHAR(30),
    PRIMARY KEY (poshipaddressid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_poshipads
ADD
  CONSTRAINT fk_1_vtiger_poshipads FOREIGN KEY (poshipaddressid) REFERENCES vtiger_purchaseorder(purchaseorderid) ON DELETE CASCADE;CREATE TABLE vtiger_purchaseordercf (
    purchaseorderid INTEGER(19) NOT NULL DEFAULT 0,
    PRIMARY KEY (purchaseorderid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_purchaseordercf
ADD
  CONSTRAINT fk_1_vtiger_purchaseordercf FOREIGN KEY (purchaseorderid) REFERENCES vtiger_purchaseorder(purchaseorderid) ON DELETE CASCADE;CREATE TABLE vtiger_invoice (
    invoiceid INTEGER(19) NOT NULL DEFAULT 0,
    subject VARCHAR(100),
    salesorderid INTEGER(19),
    customerno VARCHAR(100),
    contactid INTEGER(19),
    notes VARCHAR(100),
    invoicedate DATE,
    duedate DATE,
    invoiceterms VARCHAR(100),
    type VARCHAR(100),
    adjustment NUMERIC(25, 3),
    salescommission NUMERIC(25, 3),
    exciseduty NUMERIC(25, 3),
    subtotal NUMERIC(25, 3),
    total NUMERIC(25, 3),
    taxtype VARCHAR(25),
    discount_percent NUMERIC(25, 3),
    discount_amount NUMERIC(25, 3),
    s_h_amount NUMERIC(25, 3),
    shipping VARCHAR(100),
    accountid INTEGER(19),
    terms_conditions TEXT,
    purchaseorder VARCHAR(200),
    invoicestatus VARCHAR(200),
    invoice_no VARCHAR(100) DEFAULT NULL,
    currency_id INTEGER(19) NOT NULL DEFAULT 1,
    conversion_rate NUMERIC(10, 3) NOT NULL DEFAULT 1.000,
    compound_taxes_info TEXT,
    PRIMARY KEY (invoiceid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_invoice
ADD
  INDEX invoice_purchaseorderid_idx (invoiceid);
ALTER TABLE
  vtiger_invoice
ADD
  CONSTRAINT fk_2_vtiger_invoice FOREIGN KEY (salesorderid) REFERENCES vtiger_salesorder(salesorderid) ON DELETE CASCADE;CREATE TABLE vtiger_invoicebillads (
    invoicebilladdressid INTEGER(19) NOT NULL DEFAULT 0,
    bill_city VARCHAR(30),
    bill_code VARCHAR(30),
    bill_country VARCHAR(30),
    bill_state VARCHAR(30),
    bill_street VARCHAR(250),
    bill_pobox VARCHAR(30),
    PRIMARY KEY (invoicebilladdressid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_invoicebillads
ADD
  CONSTRAINT fk_1_vtiger_invoicebillads FOREIGN KEY (invoicebilladdressid) REFERENCES vtiger_invoice(invoiceid) ON DELETE CASCADE;CREATE TABLE vtiger_invoiceshipads (
    invoiceshipaddressid INTEGER(19) NOT NULL DEFAULT 0,
    ship_city VARCHAR(30),
    ship_code VARCHAR(30),
    ship_country VARCHAR(30),
    ship_state VARCHAR(30),
    ship_street VARCHAR(250),
    ship_pobox VARCHAR(30),
    PRIMARY KEY (invoiceshipaddressid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_invoiceshipads
ADD
  CONSTRAINT fk_1_vtiger_invoiceshipads FOREIGN KEY (invoiceshipaddressid) REFERENCES vtiger_invoice(invoiceid) ON DELETE CASCADE;CREATE TABLE vtiger_invoicecf (
    invoiceid INTEGER(19) NOT NULL DEFAULT 0,
    PRIMARY KEY (invoiceid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_invoicecf
ADD
  CONSTRAINT fk_1_vtiger_invoicecf FOREIGN KEY (invoiceid) REFERENCES vtiger_invoice(invoiceid) ON DELETE CASCADE;CREATE TABLE vtiger_activity_reminder (
    activity_id INTEGER(11) NOT NULL,
    reminder_time INTEGER(11) NOT NULL,
    reminder_sent INTEGER(2) NOT NULL,
    recurringid INTEGER(19) NOT NULL,
    PRIMARY KEY (activity_id, recurringid)
  ) Engine = InnoDB;CREATE TABLE vtiger_customview (
    cvid INTEGER(19) NOT NULL,
    viewname VARCHAR(100) NOT NULL,
    setdefault INTEGER(1) DEFAULT 0,
    setmetrics INTEGER(1) DEFAULT 0,
    entitytype VARCHAR(25) NOT NULL,
    status INTEGER(1) DEFAULT 1,
    userid INTEGER(19) DEFAULT 1,
    PRIMARY KEY (cvid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_customview
ADD
  INDEX customview_entitytype_idx (entitytype);
ALTER TABLE
  vtiger_customview
ADD
  CONSTRAINT fk_1_vtiger_customview FOREIGN KEY (entitytype) REFERENCES vtiger_tab (name) ON DELETE CASCADE;CREATE TABLE vtiger_cvcolumnlist (
    cvid INTEGER(19) NOT NULL,
    columnindex INTEGER(11) NOT NULL,
    columnname VARCHAR(250) DEFAULT '',
    PRIMARY KEY (cvid, columnindex)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_cvcolumnlist
ADD
  INDEX cvcolumnlist_columnindex_idx (columnindex);
ALTER TABLE
  vtiger_cvcolumnlist
ADD
  INDEX cvcolumnlist_cvid_idx (cvid);
ALTER TABLE
  vtiger_cvcolumnlist
ADD
  CONSTRAINT fk_1_vtiger_cvcolumnlist FOREIGN KEY (cvid) REFERENCES vtiger_customview (cvid) ON DELETE CASCADE;CREATE TABLE vtiger_cvstdfilter (
    cvid INTEGER(19) NOT NULL,
    columnname VARCHAR(250) DEFAULT '',
    stdfilter VARCHAR(250) DEFAULT '',
    startdate DATE DEFAULT NULL,
    enddate DATE DEFAULT NULL,
    PRIMARY KEY (cvid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_cvstdfilter
ADD
  INDEX cvstdfilter_cvid_idx (cvid);
ALTER TABLE
  vtiger_cvstdfilter
ADD
  CONSTRAINT fk_1_vtiger_cvstdfilter FOREIGN KEY (cvid) REFERENCES vtiger_customview (cvid) ON DELETE CASCADE;CREATE TABLE vtiger_cvadvfilter (
    cvid INTEGER(19) NOT NULL,
    columnindex INTEGER(11) NOT NULL,
    columnname VARCHAR(250) DEFAULT '',
    comparator VARCHAR(10) DEFAULT '',
    value VARCHAR(200) DEFAULT '',
    groupid INTEGER DEFAULT 1,
    column_condition VARCHAR(255) DEFAULT 'and',
    PRIMARY KEY (cvid, columnindex)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_cvadvfilter
ADD
  INDEX cvadvfilter_cvid_idx (cvid);
ALTER TABLE
  vtiger_cvadvfilter
ADD
  CONSTRAINT fk_1_vtiger_cvadvfilter FOREIGN KEY (cvid) REFERENCES vtiger_customview (cvid) ON DELETE CASCADE;CREATE TABLE vtiger_customaction (
    cvid INTEGER(19) NOT NULL,
    subject VARCHAR(250) NOT NULL,
    module VARCHAR(50) NOT NULL,
    content TEXT
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_customaction
ADD
  INDEX customaction_cvid_idx (cvid);
ALTER TABLE
  vtiger_customaction
ADD
  CONSTRAINT fk_1_vtiger_customaction FOREIGN KEY (cvid) REFERENCES vtiger_customview (cvid) ON DELETE CASCADE;CREATE TABLE vtiger_selectquery (
    queryid INTEGER(19) NOT NULL,
    startindex INTEGER(19) DEFAULT 0,
    numofobjects INTEGER(19) DEFAULT 0,
    PRIMARY KEY (queryid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_selectquery
ADD
  INDEX selectquery_queryid_idx (queryid);CREATE TABLE vtiger_selectcolumn (
    queryid INTEGER(19) NOT NULL,
    columnindex INTEGER(11) NOT NULL DEFAULT 0,
    columnname VARCHAR(250) DEFAULT '',
    PRIMARY KEY (queryid, columnindex)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_selectcolumn
ADD
  INDEX selectcolumn_queryid_idx (queryid);
ALTER TABLE
  vtiger_selectcolumn
ADD
  CONSTRAINT fk_1_vtiger_selectcolumn FOREIGN KEY (queryid) REFERENCES vtiger_selectquery (queryid) ON DELETE CASCADE;CREATE TABLE vtiger_relcriteria (
    queryid INTEGER(19) NOT NULL,
    columnindex INTEGER(11) NOT NULL,
    columnname VARCHAR(250) DEFAULT '',
    comparator VARCHAR(10) DEFAULT '',
    value VARCHAR(200) DEFAULT '',
    groupid INTEGER(11) DEFAULT 1,
    column_condition VARCHAR(256) DEFAULT 'and',
    PRIMARY KEY (queryid, columnindex)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_relcriteria
ADD
  INDEX relcriteria_queryid_idx (queryid);
ALTER TABLE
  vtiger_relcriteria
ADD
  CONSTRAINT fk_1_vtiger_relcriteria FOREIGN KEY (queryid) REFERENCES vtiger_selectquery (queryid) ON DELETE CASCADE;CREATE TABLE vtiger_reportfolder (
    folderid INTEGER(19) NOT NULL AUTO_INCREMENT,
    foldername VARCHAR(100) NOT NULL DEFAULT '',
    description VARCHAR(250) DEFAULT '',
    state VARCHAR(50) DEFAULT 'SAVED',
    PRIMARY KEY (folderid)
  ) Engine = InnoDB;CREATE TABLE vtiger_report (
    reportid INTEGER(19) NOT NULL,
    folderid INTEGER(19) NOT NULL,
    reportname VARCHAR(100) DEFAULT '',
    description VARCHAR(250) DEFAULT '',
    reporttype VARCHAR(50) DEFAULT '',
    queryid INTEGER(19) NOT NULL DEFAULT 0,
    state VARCHAR(50) DEFAULT 'SAVED',
    customizable INTEGER(1) DEFAULT 1,
    category INTEGER(11) DEFAULT 1,
    owner INTEGER(11) DEFAULT 1,
    sharingtype VARCHAR(200) DEFAULT 'Private',
    PRIMARY KEY (reportid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_report
ADD
  INDEX report_queryid_idx (queryid);
ALTER TABLE
  vtiger_report
ADD
  INDEX report_folderid_idx (folderid);
ALTER TABLE
  vtiger_report
ADD
  CONSTRAINT fk_2_vtiger_report FOREIGN KEY (queryid) REFERENCES vtiger_selectquery (queryid) ON DELETE CASCADE;CREATE TABLE vtiger_reportmodules (
    reportmodulesid INTEGER(19) NOT NULL,
    primarymodule VARCHAR(50) NOT NULL DEFAULT '',
    secondarymodules VARCHAR(250) DEFAULT '',
    PRIMARY KEY (reportmodulesid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_reportmodules
ADD
  CONSTRAINT fk_1_vtiger_reportmodules FOREIGN KEY (reportmodulesid) REFERENCES vtiger_report (reportid) ON DELETE CASCADE;CREATE TABLE vtiger_reportsortcol (
    sortcolid INTEGER(19) NOT NULL,
    reportid INTEGER(19) NOT NULL,
    columnname VARCHAR(250) DEFAULT '',
    sortorder VARCHAR(250) DEFAULT 'Asc',
    PRIMARY KEY (sortcolid, reportid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_reportsortcol
ADD
  CONSTRAINT fk_1_vtiger_reportsortcol FOREIGN KEY (reportid) REFERENCES vtiger_report (reportid) ON DELETE CASCADE;CREATE TABLE vtiger_reportdatefilter (
    datefilterid INTEGER(19) NOT NULL,
    datecolumnname VARCHAR(250) DEFAULT '',
    datefilter VARCHAR(250) DEFAULT '',
    startdate DATE DEFAULT NULL,
    enddate DATE DEFAULT NULL,
    PRIMARY KEY (datefilterid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_reportdatefilter
ADD
  INDEX reportdatefilter_datefilterid_idx (datefilterid);
ALTER TABLE
  vtiger_reportdatefilter
ADD
  CONSTRAINT fk_1_vtiger_reportdatefilter FOREIGN KEY (datefilterid) REFERENCES vtiger_report (reportid) ON DELETE CASCADE;CREATE TABLE vtiger_reportsummary (
    reportsummaryid INTEGER(19) NOT NULL,
    summarytype INTEGER(19) NOT NULL,
    columnname VARCHAR(250) NOT NULL DEFAULT '',
    PRIMARY KEY (reportsummaryid, summarytype, columnname)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_reportsummary
ADD
  INDEX reportsummary_reportsummaryid_idx (reportsummaryid);
ALTER TABLE
  vtiger_reportsummary
ADD
  CONSTRAINT fk_1_vtiger_reportsummary FOREIGN KEY (reportsummaryid) REFERENCES vtiger_report (reportid) ON DELETE CASCADE;CREATE TABLE vtiger_reporttype (
    reportid INTEGER(10) NOT NULL,
    data TEXT,
    PRIMARY KEY (reportid)
  );
ALTER TABLE
  vtiger_reporttype
ADD
  CONSTRAINT fk_1_vtiger_reporttype FOREIGN KEY (reportid) REFERENCES vtiger_report (reportid) ON DELETE CASCADE;CREATE TABLE vtiger_usageunit (
    usageunitid INTEGER(19) NOT NULL AUTO_INCREMENT,
    usageunit VARCHAR(200) NOT NULL,
    presence INTEGER(1) NOT NULL DEFAULT 1,
    picklist_valueid INTEGER(19) NOT NULL DEFAULT 0,
    sortorderid INTEGER(11),
    PRIMARY KEY (usageunitid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_usageunit
ADD
  UNIQUE INDEX usageunit_usageunit_idx (usageunit);CREATE TABLE vtiger_glacct (
    glacctid INTEGER(19) NOT NULL AUTO_INCREMENT,
    glacct VARCHAR(200) NOT NULL,
    presence INTEGER(1) NOT NULL DEFAULT 1,
    picklist_valueid INTEGER(19) NOT NULL DEFAULT 0,
    sortorderid INTEGER(11),
    PRIMARY KEY (glacctid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_glacct
ADD
  UNIQUE INDEX glacct_glacct_idx (glacct);CREATE TABLE vtiger_quotestage (
    quotestageid INTEGER(19) NOT NULL AUTO_INCREMENT,
    quotestage VARCHAR(200) NOT NULL,
    presence INTEGER(1) NOT NULL DEFAULT 1,
    picklist_valueid INTEGER(19) NOT NULL DEFAULT 0,
    sortorderid INTEGER(11),
    PRIMARY KEY (quotestageid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_quotestage
ADD
  UNIQUE INDEX quotestage_quotestage_idx (quotestage);CREATE TABLE vtiger_invoicestatus (
    invoicestatusid INTEGER(19) NOT NULL AUTO_INCREMENT,
    invoicestatus VARCHAR(200) NOT NULL,
    presence INTEGER(1) NOT NULL DEFAULT 1,
    picklist_valueid INTEGER(19) NOT NULL DEFAULT 0,
    sortorderid INTEGER(11),
    PRIMARY KEY (invoicestatusid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_invoicestatus
ADD
  UNIQUE INDEX invoicestatus_invoiestatus_idx (invoicestatus);CREATE TABLE vtiger_postatus (
    postatusid INTEGER(19) NOT NULL AUTO_INCREMENT,
    postatus VARCHAR(200) NOT NULL,
    presence INTEGER(1) NOT NULL DEFAULT 1,
    picklist_valueid INTEGER(19) NOT NULL DEFAULT 0,
    sortorderid INTEGER(11),
    PRIMARY KEY (postatusid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_postatus
ADD
  UNIQUE INDEX postatus_postatus_idx (postatus);CREATE TABLE vtiger_sostatus (
    sostatusid INTEGER(19) NOT NULL AUTO_INCREMENT,
    sostatus VARCHAR(200) NOT NULL,
    presence INTEGER(1) NOT NULL DEFAULT 1,
    picklist_valueid INTEGER(19) NOT NULL DEFAULT 0,
    sortorderid INTEGER(11),
    PRIMARY KEY (sostatusid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_sostatus
ADD
  UNIQUE INDEX sostatus_sostatus_idx (sostatus);CREATE TABLE vtiger_carrier (
    carrierid INTEGER(19) NOT NULL AUTO_INCREMENT,
    carrier VARCHAR(200) NOT NULL,
    presence INTEGER(1) NOT NULL DEFAULT 1,
    picklist_valueid INTEGER(19) NOT NULL DEFAULT 0,
    sortorderid INTEGER(11),
    PRIMARY KEY (carrierid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_carrier
ADD
  UNIQUE INDEX carrier_carrier_idx (carrier);CREATE TABLE vtiger_taxclass (
    taxclassid INTEGER(19) NOT NULL AUTO_INCREMENT,
    taxclass VARCHAR(200) NOT NULL,
    sortorderid INTEGER(19) NOT NULL DEFAULT 0,
    presence INTEGER(1) NOT NULL DEFAULT 1,
    PRIMARY KEY (taxclassid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_taxclass
ADD
  UNIQUE INDEX taxclass_carrier_idx (taxclass);CREATE TABLE vtiger_organizationdetails (
    organization_id INTEGER(11) NOT NULL,
    organizationname VARCHAR(60),
    address VARCHAR(150),
    city VARCHAR(100),
    state VARCHAR(100),
    country VARCHAR(100),
    code VARCHAR(30),
    phone VARCHAR(30),
    fax VARCHAR(30),
    website VARCHAR(100),
    logoname VARCHAR(50),
    logo TEXT,
    vatid VARCHAR(100),
    PRIMARY KEY (organization_id)
  ) Engine = InnoDB;CREATE TABLE vtiger_recurringtype (
    recurringeventid INTEGER(19) NOT NULL AUTO_INCREMENT,
    recurringtype VARCHAR(200) NOT NULL,
    sortorderid INTEGER(19) NOT NULL DEFAULT 0,
    presence INTEGER(1) NOT NULL DEFAULT 1,
    PRIMARY KEY (recurringeventid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_recurringtype
ADD
  UNIQUE INDEX recurringtype_status_idx (recurringtype);CREATE TABLE vtiger_recurringevents (
    recurringid INTEGER(19) NOT NULL AUTO_INCREMENT,
    activityid INTEGER(19) NOT NULL,
    recurringdate DATE,
    recurringtype VARCHAR(30),
    recurringfreq INTEGER(19),
    recurringinfo VARCHAR(50),
    PRIMARY KEY (recurringid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_recurringevents
ADD
  CONSTRAINT fk_1_vtiger_recurringevents FOREIGN KEY (activityid) REFERENCES vtiger_activity(activityid) ON DELETE CASCADE;CREATE TABLE vtiger_inventorynotification (
    notificationid INTEGER(19) NOT NULL AUTO_INCREMENT,
    notificationname VARCHAR(200),
    notificationsubject VARCHAR(200),
    notificationbody TEXT,
    label VARCHAR(50),
    status VARCHAR(30),
    PRIMARY KEY (notificationid)
  ) Engine = InnoDB;CREATE TABLE vtiger_inventory_tandc (
    id INTEGER(19) NOT NULL,
    type VARCHAR(30) NOT NULL,
    tandc TEXT,
    PRIMARY KEY (id)
  ) Engine = InnoDB;CREATE TABLE vtiger_convertleadmapping (
    cfmid INTEGER(19) NOT NULL AUTO_INCREMENT,
    leadfid INTEGER(19) NOT NULL,
    accountfid INTEGER(19),
    contactfid INTEGER(19),
    potentialfid INTEGER(19),
    editable INTEGER(19) DEFAULT 1,
    PRIMARY KEY (cfmid)
  ) Engine = InnoDB;CREATE TABLE vtiger_actionmapping (
    actionid INTEGER(19) NOT NULL,
    actionname VARCHAR(200) NOT NULL,
    securitycheck INTEGER(19),
    PRIMARY KEY (actionid, actionname)
  ) Engine = InnoDB;CREATE TABLE vtiger_org_share_action2tab (
    share_action_id INTEGER(19) NOT NULL,
    tabid INTEGER(19) NOT NULL,
    PRIMARY KEY (share_action_id, tabid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_org_share_action2tab
ADD
  CONSTRAINT fk_2_vtiger_org_share_action2tab FOREIGN KEY (tabid) REFERENCES vtiger_tab(tabid) ON DELETE CASCADE;CREATE TABLE vtiger_datashare_module_rel (
    shareid INTEGER(19) NOT NULL,
    tabid INTEGER(19) NOT NULL,
    relationtype VARCHAR(200),
    PRIMARY KEY (shareid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_datashare_module_rel
ADD
  INDEX idx_datashare_module_rel_tabid (tabid);
ALTER TABLE
  vtiger_datashare_module_rel
ADD
  CONSTRAINT fk_1_vtiger_datashare_module_rel FOREIGN KEY (tabid) REFERENCES vtiger_tab(tabid) ON DELETE CASCADE;CREATE TABLE vtiger_datashare_grp2grp (
    shareid INTEGER(19) NOT NULL,
    share_groupid INTEGER(19),
    to_groupid INTEGER(19),
    permission INTEGER(19),
    PRIMARY KEY (shareid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_datashare_grp2grp
ADD
  INDEX datashare_grp2grp_share_groupid_idx (share_groupid);
ALTER TABLE
  vtiger_datashare_grp2grp
ADD
  INDEX datashare_grp2grp_to_groupid_idx (to_groupid);
ALTER TABLE
  vtiger_datashare_grp2grp
ADD
  CONSTRAINT fk_3_vtiger_datashare_grp2grp FOREIGN KEY (to_groupid) REFERENCES vtiger_groups(groupid) ON DELETE CASCADE;CREATE TABLE vtiger_datashare_grp2role (
    shareid INTEGER(19) NOT NULL,
    share_groupid INTEGER(19),
    to_roleid VARCHAR(255),
    permission INTEGER(19),
    PRIMARY KEY (shareid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_datashare_grp2role
ADD
  INDEX idx_datashare_grp2role_share_groupid (share_groupid);
ALTER TABLE
  vtiger_datashare_grp2role
ADD
  INDEX idx_datashare_grp2role_to_roleid (to_roleid);
ALTER TABLE
  vtiger_datashare_grp2role
ADD
  CONSTRAINT fk_3_vtiger_datashare_grp2role FOREIGN KEY (to_roleid) REFERENCES vtiger_role(roleid) ON DELETE CASCADE;CREATE TABLE vtiger_datashare_grp2rs (
    shareid INTEGER(19) NOT NULL,
    share_groupid INTEGER(19),
    to_roleandsubid VARCHAR(255),
    permission INTEGER(19),
    PRIMARY KEY (shareid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_datashare_grp2rs
ADD
  INDEX datashare_grp2rs_share_groupid_idx (share_groupid);
ALTER TABLE
  vtiger_datashare_grp2rs
ADD
  INDEX datashare_grp2rs_to_roleandsubid_idx (to_roleandsubid);
ALTER TABLE
  vtiger_datashare_grp2rs
ADD
  CONSTRAINT fk_3_vtiger_datashare_grp2rs FOREIGN KEY (to_roleandsubid) REFERENCES vtiger_role(roleid) ON DELETE CASCADE;CREATE TABLE vtiger_datashare_role2group (
    shareid INTEGER(19) NOT NULL,
    share_roleid VARCHAR(255),
    to_groupid INTEGER(19),
    permission INTEGER(19),
    PRIMARY KEY (shareid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_datashare_role2group
ADD
  INDEX idx_datashare_role2group_share_roleid (share_roleid);
ALTER TABLE
  vtiger_datashare_role2group
ADD
  INDEX idx_datashare_role2group_to_groupid (to_groupid);
ALTER TABLE
  vtiger_datashare_role2group
ADD
  CONSTRAINT fk_3_vtiger_datashare_role2group FOREIGN KEY (share_roleid) REFERENCES vtiger_role(roleid) ON DELETE CASCADE;CREATE TABLE vtiger_datashare_role2role (
    shareid INTEGER(19) NOT NULL,
    share_roleid VARCHAR(255),
    to_roleid VARCHAR(255),
    permission INTEGER(19),
    PRIMARY KEY (shareid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_datashare_role2role
ADD
  INDEX datashare_role2role_share_roleid_idx (share_roleid);
ALTER TABLE
  vtiger_datashare_role2role
ADD
  INDEX datashare_role2role_to_roleid_idx (to_roleid);
ALTER TABLE
  vtiger_datashare_role2role
ADD
  CONSTRAINT fk_3_vtiger_datashare_role2role FOREIGN KEY (to_roleid) REFERENCES vtiger_role(roleid) ON DELETE CASCADE;CREATE TABLE vtiger_datashare_role2rs (
    shareid INTEGER(19) NOT NULL,
    share_roleid VARCHAR(255),
    to_roleandsubid VARCHAR(255),
    permission INTEGER(19),
    PRIMARY KEY (shareid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_datashare_role2rs
ADD
  INDEX datashare_role2s_share_roleid_idx (share_roleid);
ALTER TABLE
  vtiger_datashare_role2rs
ADD
  INDEX datashare_role2s_to_roleandsubid_idx (to_roleandsubid);
ALTER TABLE
  vtiger_datashare_role2rs
ADD
  CONSTRAINT fk_3_vtiger_datashare_role2rs FOREIGN KEY (to_roleandsubid) REFERENCES vtiger_role(roleid) ON DELETE CASCADE;CREATE TABLE vtiger_datashare_rs2grp (
    shareid INTEGER(19) NOT NULL,
    share_roleandsubid VARCHAR(255),
    to_groupid INTEGER(19),
    permission INTEGER(19),
    PRIMARY KEY (shareid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_datashare_rs2grp
ADD
  INDEX datashare_rs2grp_share_roleandsubid_idx (share_roleandsubid);
ALTER TABLE
  vtiger_datashare_rs2grp
ADD
  INDEX datashare_rs2grp_to_groupid_idx (to_groupid);
ALTER TABLE
  vtiger_datashare_rs2grp
ADD
  CONSTRAINT fk_3_vtiger_datashare_rs2grp FOREIGN KEY (share_roleandsubid) REFERENCES vtiger_role(roleid) ON DELETE CASCADE;CREATE TABLE vtiger_datashare_rs2role (
    shareid INTEGER(19) NOT NULL,
    share_roleandsubid VARCHAR(255),
    to_roleid VARCHAR(255),
    permission INTEGER(19),
    PRIMARY KEY (shareid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_datashare_rs2role
ADD
  INDEX datashare_rs2role_share_roleandsubid_idx (share_roleandsubid);
ALTER TABLE
  vtiger_datashare_rs2role
ADD
  INDEX datashare_rs2role_to_roleid_idx (to_roleid);
ALTER TABLE
  vtiger_datashare_rs2role
ADD
  CONSTRAINT fk_3_vtiger_datashare_rs2role FOREIGN KEY (to_roleid) REFERENCES vtiger_role(roleid) ON DELETE CASCADE;CREATE TABLE vtiger_datashare_rs2rs (
    shareid INTEGER(19) NOT NULL,
    share_roleandsubid VARCHAR(255),
    to_roleandsubid VARCHAR(255),
    permission INTEGER(19),
    PRIMARY KEY (shareid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_datashare_rs2rs
ADD
  INDEX datashare_rs2rs_share_roleandsubid_idx (share_roleandsubid);
ALTER TABLE
  vtiger_datashare_rs2rs
ADD
  INDEX idx_datashare_rs2rs_to_roleandsubid_idx (to_roleandsubid);
ALTER TABLE
  vtiger_datashare_rs2rs
ADD
  CONSTRAINT fk_3_vtiger_datashare_rs2rs FOREIGN KEY (to_roleandsubid) REFERENCES vtiger_role(roleid) ON DELETE CASCADE;CREATE TABLE vtiger_datashare_relatedmodules (
    datashare_relatedmodule_id INTEGER(19) NOT NULL,
    tabid INTEGER(19),
    relatedto_tabid INTEGER(19),
    PRIMARY KEY (datashare_relatedmodule_id)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_datashare_relatedmodules
ADD
  INDEX datashare_relatedmodules_tabid_idx (tabid);
ALTER TABLE
  vtiger_datashare_relatedmodules
ADD
  INDEX datashare_relatedmodules_relatedto_tabid_idx (relatedto_tabid);
ALTER TABLE
  vtiger_datashare_relatedmodules
ADD
  CONSTRAINT fk_2_vtiger_datashare_relatedmodules FOREIGN KEY (tabid) REFERENCES vtiger_tab(tabid) ON DELETE CASCADE;CREATE TABLE vtiger_datashare_relatedmodule_permission (
    shareid INTEGER(19) NOT NULL,
    datashare_relatedmodule_id INTEGER(19) NOT NULL,
    permission INTEGER(19),
    PRIMARY KEY (shareid, datashare_relatedmodule_id)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_datashare_relatedmodule_permission
ADD
  INDEX datashare_relatedmodule_permission_shareid_permissions_idx (shareid, permission);CREATE TABLE vtiger_parenttab (
    parenttabid INTEGER(19) NOT NULL,
    parenttab_label VARCHAR(100) NOT NULL,
    sequence INTEGER(10) NOT NULL,
    visible INTEGER(2) NOT NULL DEFAULT 0,
    PRIMARY KEY (parenttabid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_parenttab
ADD
  INDEX parenttab_parenttabid_parenttabl_label_visible_idx (parenttabid, parenttab_label, visible);CREATE TABLE vtiger_tmp_read_user_sharing_per (
    userid INTEGER(11) NOT NULL,
    tabid INTEGER(11) NOT NULL,
    shareduserid INTEGER(11) NOT NULL,
    PRIMARY KEY (userid, tabid, shareduserid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_tmp_read_user_sharing_per
ADD
  INDEX tmp_read_user_sharing_per_userid_shareduserid_idx (userid, shareduserid);
ALTER TABLE
  vtiger_tmp_read_user_sharing_per
ADD
  CONSTRAINT fk_3_vtiger_tmp_read_user_sharing_per FOREIGN KEY (userid) REFERENCES vtiger_users(id) ON DELETE CASCADE;CREATE TABLE vtiger_tmp_read_group_sharing_per (
    userid INTEGER(11) NOT NULL,
    tabid INTEGER(11) NOT NULL,
    sharedgroupid INTEGER(11) NOT NULL,
    PRIMARY KEY (userid, tabid, sharedgroupid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_tmp_read_group_sharing_per
ADD
  INDEX tmp_read_group_sharing_per_userid_sharedgroupid_idx (userid, sharedgroupid);
ALTER TABLE
  vtiger_tmp_read_group_sharing_per
ADD
  CONSTRAINT fk_3_vtiger_tmp_read_group_sharing_per FOREIGN KEY (userid) REFERENCES vtiger_users(id) ON DELETE CASCADE;CREATE TABLE vtiger_tmp_write_user_sharing_per (
    userid INTEGER(11) NOT NULL,
    tabid INTEGER(11) NOT NULL,
    shareduserid INTEGER(11) NOT NULL,
    PRIMARY KEY (userid, tabid, shareduserid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_tmp_write_user_sharing_per
ADD
  INDEX tmp_write_user_sharing_per_userid_shareduserid_idx (userid, shareduserid);
ALTER TABLE
  vtiger_tmp_write_user_sharing_per
ADD
  CONSTRAINT fk_3_vtiger_tmp_write_user_sharing_per FOREIGN KEY (userid) REFERENCES vtiger_users(id) ON DELETE CASCADE;CREATE TABLE vtiger_tmp_write_group_sharing_per (
    userid INTEGER(11) NOT NULL,
    tabid INTEGER(11) NOT NULL,
    sharedgroupid INTEGER(11) NOT NULL,
    PRIMARY KEY (userid, tabid, sharedgroupid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_tmp_write_group_sharing_per
ADD
  INDEX tmp_write_group_sharing_per_UK1 (userid, sharedgroupid);
ALTER TABLE
  vtiger_tmp_write_group_sharing_per
ADD
  CONSTRAINT fk_3_vtiger_tmp_write_group_sharing_per FOREIGN KEY (userid) REFERENCES vtiger_users(id) ON DELETE CASCADE;CREATE TABLE vtiger_tmp_read_user_rel_sharing_per (
    userid INTEGER(11) NOT NULL,
    tabid INTEGER(11) NOT NULL,
    relatedtabid INTEGER(11) NOT NULL,
    shareduserid INTEGER(11) NOT NULL,
    PRIMARY KEY (userid, tabid, relatedtabid, shareduserid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_tmp_read_user_rel_sharing_per
ADD
  INDEX tmp_read_user_rel_sharing_per_userid_shared_reltabid_idx (userid, shareduserid, relatedtabid);
ALTER TABLE
  vtiger_tmp_read_user_rel_sharing_per
ADD
  CONSTRAINT fk_4_vtiger_tmp_read_user_rel_sharing_per FOREIGN KEY (userid) REFERENCES vtiger_users(id) ON DELETE CASCADE;CREATE TABLE vtiger_tmp_read_group_rel_sharing_per (
    userid INTEGER(11) NOT NULL,
    tabid INTEGER(11) NOT NULL,
    relatedtabid INTEGER(11) NOT NULL,
    sharedgroupid INTEGER(11) NOT NULL,
    PRIMARY KEY (userid, tabid, relatedtabid, sharedgroupid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_tmp_read_group_rel_sharing_per
ADD
  INDEX tmp_read_group_rel_sharing_per_userid_sharedgroupid_tabid (userid, sharedgroupid, tabid);
ALTER TABLE
  vtiger_tmp_read_group_rel_sharing_per
ADD
  CONSTRAINT fk_4_vtiger_tmp_read_group_rel_sharing_per FOREIGN KEY (userid) REFERENCES vtiger_users(id) ON DELETE CASCADE;CREATE TABLE vtiger_tmp_write_user_rel_sharing_per (
    userid INTEGER(11) NOT NULL,
    tabid INTEGER(11) NOT NULL,
    relatedtabid INTEGER(11) NOT NULL,
    shareduserid INTEGER(11) NOT NULL,
    PRIMARY KEY (userid, tabid, relatedtabid, shareduserid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_tmp_write_user_rel_sharing_per
ADD
  INDEX tmp_write_user_rel_sharing_per_userid_sharduserid_tabid_idx (userid, shareduserid, tabid);
ALTER TABLE
  vtiger_tmp_write_user_rel_sharing_per
ADD
  CONSTRAINT fk_4_vtiger_tmp_write_user_rel_sharing_per FOREIGN KEY (userid) REFERENCES vtiger_users(id) ON DELETE CASCADE;CREATE TABLE vtiger_tmp_write_group_rel_sharing_per (
    userid INTEGER(11) NOT NULL,
    tabid INTEGER(11) NOT NULL,
    relatedtabid INTEGER(11) NOT NULL,
    sharedgroupid INTEGER(11) NOT NULL,
    PRIMARY KEY (userid, tabid, relatedtabid, sharedgroupid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_tmp_write_group_rel_sharing_per
ADD
  INDEX tmp_write_group_rel_sharing_per_userid_sharedgroupid_tabid_idx (userid, sharedgroupid, tabid);
ALTER TABLE
  vtiger_tmp_write_group_rel_sharing_per
ADD
  CONSTRAINT fk_4_vtiger_tmp_write_group_rel_sharing_per FOREIGN KEY (userid) REFERENCES vtiger_users(id) ON DELETE CASCADE;CREATE TABLE vtiger_parenttabrel (
    parenttabid INTEGER(19) NOT NULL,
    tabid INTEGER(19) NOT NULL,
    sequence INTEGER(3) NOT NULL
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_parenttabrel
ADD
  INDEX parenttabrel_tabid_parenttabid_idx (tabid, parenttabid);
ALTER TABLE
  vtiger_parenttabrel
ADD
  CONSTRAINT fk_1_vtiger_parenttabrel FOREIGN KEY (tabid) REFERENCES vtiger_tab(tabid) ON DELETE CASCADE;
ALTER TABLE
  vtiger_parenttabrel
ADD
  CONSTRAINT fk_2_vtiger_parenttabrel FOREIGN KEY (parenttabid) REFERENCES vtiger_parenttab(parenttabid) ON DELETE CASCADE;CREATE TABLE vtiger_campaigntype (
    campaigntypeid INTEGER(19) NOT NULL AUTO_INCREMENT,
    campaigntype VARCHAR(200) NOT NULL,
    presence INTEGER(1) NOT NULL DEFAULT 1,
    picklist_valueid INTEGER(19) NOT NULL DEFAULT 0,
    sortorderid INTEGER(11),
    PRIMARY KEY (campaigntypeid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_campaigntype
ADD
  UNIQUE INDEX campaigntype_campaigntype_idx (campaigntype);CREATE TABLE vtiger_campaignstatus (
    campaignstatusid INTEGER(19) NOT NULL AUTO_INCREMENT,
    campaignstatus VARCHAR(200) NOT NULL,
    presence INTEGER(1) NOT NULL DEFAULT 1,
    picklist_valueid INTEGER(19) NOT NULL DEFAULT 0,
    sortorderid INTEGER(11),
    PRIMARY KEY (campaignstatusid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_campaignstatus
ADD
  INDEX campaignstatus_campaignstatus_idx (campaignstatus);CREATE TABLE vtiger_expectedresponse (
    expectedresponseid INTEGER(19) NOT NULL AUTO_INCREMENT,
    expectedresponse VARCHAR(200) NOT NULL,
    presence INTEGER(1) NOT NULL DEFAULT 1,
    picklist_valueid INTEGER(19) NOT NULL DEFAULT 0,
    sortorderid INTEGER(11),
    PRIMARY KEY (expectedresponseid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_expectedresponse
ADD
  UNIQUE INDEX CampaignExpRes_UK01 (expectedresponse);CREATE TABLE vtiger_picklist (
    picklistid INTEGER(11) NOT NULL AUTO_INCREMENT,
    name VARCHAR(200) NOT NULL,
    PRIMARY KEY (picklistid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_picklist
ADD
  UNIQUE INDEX picklist_name_idx (name);CREATE TABLE vtiger_role2picklist (
    roleid VARCHAR(255) NOT NULL,
    picklistvalueid INTEGER(11) NOT NULL,
    picklistid INTEGER(11) NOT NULL,
    sortid INTEGER(11),
    PRIMARY KEY (roleid, picklistvalueid, picklistid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_role2picklist
ADD
  INDEX role2picklist_roleid_picklistid_idx (roleid, picklistid, picklistvalueid);
ALTER TABLE
  vtiger_role2picklist
ADD
  CONSTRAINT fk_1_vtiger_role2picklist FOREIGN KEY (roleid) REFERENCES vtiger_role (roleid) ON DELETE CASCADE;
ALTER TABLE
  vtiger_role2picklist
ADD
  CONSTRAINT fk_2_vtiger_role2picklist FOREIGN KEY (picklistid) REFERENCES vtiger_picklist (picklistid) ON DELETE CASCADE;CREATE TABLE vtiger_portal (
    portalid INTEGER(19) NOT NULL,
    portalname VARCHAR(200) NOT NULL,
    portalurl VARCHAR(255) NOT NULL,
    sequence INTEGER(3) NOT NULL,
    setdefault INTEGER(3) NOT NULL DEFAULT 0,
    PRIMARY KEY (portalid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_portal
ADD
  INDEX portal_portalname_idx (portalname);CREATE TABLE vtiger_soapservice (
    id INTEGER(19) DEFAULT NULL,
    type VARCHAR(25) DEFAULT NULL,
    sessionid VARCHAR(100) DEFAULT NULL
  ) Engine = InnoDB;CREATE TABLE vtiger_announcement (
    creatorid INTEGER(19) NOT NULL,
    announcement TEXT,
    title VARCHAR(255),
    time TIMESTAMP,
    PRIMARY KEY (creatorid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_announcement
ADD
  INDEX announcement_creatorid_idx (creatorid);CREATE TABLE vtiger_freetags (
    id INTEGER(19) NOT NULL,
    tag VARCHAR(50) NOT NULL DEFAULT '',
    raw_tag VARCHAR(50) NOT NULL DEFAULT '',
    visibility VARCHAR(100) NOT NULL DEFAULT 'PRIVATE',
    owner INTEGER(19) NOT NULL,
    PRIMARY KEY (id)
  ) Engine = InnoDB;CREATE TABLE vtiger_freetagged_objects (
    tag_id INTEGER(20) NOT NULL DEFAULT 0,
    tagger_id INTEGER(20) NOT NULL DEFAULT 0,
    object_id INTEGER(20) NOT NULL DEFAULT 0,
    tagged_on TIMESTAMP NOT NULL,
    module VARCHAR(50) NOT NULL DEFAULT '',
    PRIMARY KEY (tag_id, tagger_id, object_id)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_freetagged_objects
ADD
  INDEX freetagged_objects_tag_id_tagger_id_object_id_idx (tag_id, tagger_id, object_id);CREATE TABLE vtiger_emaildetails (
    emailid INTEGER(19) NOT NULL,
    from_email VARCHAR(50) NOT NULL DEFAULT '',
    to_email TEXT,
    cc_email TEXT,
    bcc_email TEXT,
    assigned_user_email VARCHAR(50) NOT NULL DEFAULT '',
    idlists TEXT,
    email_flag VARCHAR(50) NOT NULL DEFAULT '',
    PRIMARY KEY (emailid)
  ) Engine = InnoDB;CREATE TABLE vtiger_invitees (
    activityid INTEGER(19) NOT NULL,
    inviteeid INTEGER(19) NOT NULL,
    status VARCHAR(50),
    PRIMARY KEY (activityid, inviteeid)
  ) Engine = InnoDB;CREATE TABLE vtiger_inventorytaxinfo (
    taxid INTEGER(3) NOT NULL,
    taxname VARCHAR(50),
    taxlabel VARCHAR(50),
    percentage NUMERIC(7, 3),
    deleted INTEGER(1),
    PRIMARY KEY (taxid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_inventorytaxinfo
ADD
  INDEX inventorytaxinfo_taxname_idx (taxname);CREATE TABLE vtiger_producttaxrel (
    productid INTEGER(11) NOT NULL,
    taxid INTEGER(3) NOT NULL,
    taxpercentage NUMERIC(7, 3),
    regions TEXT
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_producttaxrel
ADD
  INDEX producttaxrel_productid_idx (productid);
ALTER TABLE
  vtiger_producttaxrel
ADD
  INDEX producttaxrel_taxid_idx (taxid);CREATE TABLE vtiger_status (
    statusid INTEGER(19) NOT NULL AUTO_INCREMENT,
    status VARCHAR(200) NOT NULL,
    presence INTEGER(1) NOT NULL DEFAULT 1,
    picklist_valueid INTEGER(19) NOT NULL DEFAULT 0,
    PRIMARY KEY (statusid)
  ) Engine = InnoDB;CREATE TABLE vtiger_activity_view (
    activity_viewid INTEGER(19) NOT NULL AUTO_INCREMENT,
    activity_view VARCHAR(200) NOT NULL,
    sortorderid INTEGER(19) NOT NULL DEFAULT 0,
    presence INTEGER(1) NOT NULL DEFAULT 1,
    PRIMARY KEY (activity_viewid)
  ) Engine = InnoDB;CREATE TABLE vtiger_lead_view (
    lead_viewid INTEGER(19) NOT NULL AUTO_INCREMENT,
    lead_view VARCHAR(200) NOT NULL,
    sortorderid INTEGER(19) NOT NULL DEFAULT 0,
    presence INTEGER(1) NOT NULL DEFAULT 1,
    PRIMARY KEY (lead_viewid)
  ) Engine = InnoDB;CREATE TABLE vtiger_date_format (
    date_formatid INTEGER(19) NOT NULL AUTO_INCREMENT,
    date_format VARCHAR(200) NOT NULL,
    sortorderid INTEGER(19) NOT NULL DEFAULT 0,
    presence INTEGER(1) NOT NULL DEFAULT 1,
    PRIMARY KEY (date_formatid)
  ) Engine = InnoDB;CREATE TABLE vtiger_postatushistory (
    historyid INTEGER(19) NOT NULL AUTO_INCREMENT,
    purchaseorderid INTEGER(19) NOT NULL,
    vendorname VARCHAR(100),
    total NUMERIC,
    postatus VARCHAR(200),
    lastmodified DATETIME,
    PRIMARY KEY (historyid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_postatushistory
ADD
  INDEX postatushistory_purchaseorderid_idx (purchaseorderid);
ALTER TABLE
  vtiger_postatushistory
ADD
  CONSTRAINT fk_1_vtiger_postatushistory FOREIGN KEY (purchaseorderid) REFERENCES vtiger_purchaseorder(purchaseorderid) ON DELETE CASCADE;CREATE TABLE vtiger_sostatushistory (
    historyid INTEGER(19) NOT NULL AUTO_INCREMENT,
    salesorderid INTEGER(19) NOT NULL,
    accountname VARCHAR(100),
    total NUMERIC,
    sostatus VARCHAR(200),
    lastmodified DATETIME,
    PRIMARY KEY (historyid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_sostatushistory
ADD
  INDEX sostatushistory_salesorderid_idx (salesorderid);
ALTER TABLE
  vtiger_sostatushistory
ADD
  CONSTRAINT fk_1_vtiger_sostatushistory FOREIGN KEY (salesorderid) REFERENCES vtiger_salesorder(salesorderid) ON DELETE CASCADE;CREATE TABLE vtiger_quotestagehistory (
    historyid INTEGER(19) NOT NULL AUTO_INCREMENT,
    quoteid INTEGER(19) NOT NULL,
    accountname VARCHAR(100),
    total NUMERIC,
    quotestage VARCHAR(200),
    lastmodified DATETIME,
    PRIMARY KEY (historyid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_quotestagehistory
ADD
  INDEX quotestagehistory_quoteid_idx (quoteid);
ALTER TABLE
  vtiger_quotestagehistory
ADD
  CONSTRAINT fk_1_vtiger_quotestagehistory FOREIGN KEY (quoteid) REFERENCES vtiger_quotes(quoteid) ON DELETE CASCADE;CREATE TABLE vtiger_invoicestatushistory (
    historyid INTEGER(19) NOT NULL AUTO_INCREMENT,
    invoiceid INTEGER(19) NOT NULL,
    accountname VARCHAR(100),
    total NUMERIC,
    invoicestatus VARCHAR(200),
    lastmodified DATETIME,
    PRIMARY KEY (historyid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_invoicestatushistory
ADD
  INDEX invoicestatushistory_invoiceid_idx (invoiceid);
ALTER TABLE
  vtiger_invoicestatushistory
ADD
  CONSTRAINT fk_1_vtiger_invoicestatushistory FOREIGN KEY (invoiceid) REFERENCES vtiger_invoice(invoiceid) ON DELETE CASCADE;CREATE TABLE vtiger_audit_trial (
    auditid INTEGER(19) NOT NULL,
    userid INTEGER(19),
    module VARCHAR(255),
    action VARCHAR(255),
    recordid VARCHAR(20),
    actiondate DATETIME,
    PRIMARY KEY (auditid)
  ) Engine = InnoDB;CREATE TABLE vtiger_inventoryproductrel (
    id INTEGER(19),
    productid INTEGER(19),
    sequence_no INTEGER(4),
    quantity NUMERIC(25, 3),
    listprice NUMERIC(25, 3),
    discount_percent NUMERIC(7, 3),
    discount_amount NUMERIC(25, 3),
    comment TEXT,
    description TEXT,
    incrementondel INTEGER(11) NOT NULL DEFAULT 0,
    lineitem_id INTEGER(11) NOT NULL AUTO_INCREMENT,
    PRIMARY KEY (lineitem_id)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_inventoryproductrel
ADD
  INDEX inventoryproductrel_id_idx (id);
ALTER TABLE
  vtiger_inventoryproductrel
ADD
  INDEX inventoryproductrel_productid_idx (productid);CREATE TABLE vtiger_shippingtaxinfo (
    taxid INTEGER(3) NOT NULL,
    taxname VARCHAR(50),
    taxlabel VARCHAR(50),
    percentage NUMERIC(7, 3),
    deleted INTEGER(1),
    PRIMARY KEY (taxid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_shippingtaxinfo
ADD
  INDEX shippingtaxinfo_taxname_idx (taxname);CREATE TABLE vtiger_inventoryshippingrel (id INTEGER(19)) Engine = InnoDB;
ALTER TABLE
  vtiger_inventoryshippingrel
ADD
  INDEX inventoryishippingrel_id_idx (id);CREATE TABLE vtiger_salesmanattachmentsrel (
    smid INTEGER(19) NOT NULL DEFAULT 0,
    attachmentsid INTEGER(19) NOT NULL DEFAULT 0,
    PRIMARY KEY (smid, attachmentsid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_salesmanattachmentsrel
ADD
  INDEX salesmanattachmentsrel_smid_idx (smid);
ALTER TABLE
  vtiger_salesmanattachmentsrel
ADD
  INDEX salesmanattachmentsrel_attachmentsid_idx (attachmentsid);
ALTER TABLE
  vtiger_salesmanattachmentsrel
ADD
  CONSTRAINT fk_2_vtiger_salesmanattachmentsrel FOREIGN KEY (attachmentsid) REFERENCES vtiger_attachments(attachmentsid) ON DELETE CASCADE;CREATE TABLE vtiger_entityname (
    tabid INTEGER(19) NOT NULL DEFAULT 0,
    modulename VARCHAR(50) NOT NULL,
    tablename VARCHAR(100) NOT NULL,
    fieldname VARCHAR(150) NOT NULL,
    entityidfield VARCHAR(150) NOT NULL,
    entityidcolumn VARCHAR(150) NOT NULL,
    PRIMARY KEY (tabid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_entityname
ADD
  INDEX entityname_tabid_idx (tabid);
ALTER TABLE
  vtiger_entityname
ADD
  CONSTRAINT fk_1_vtiger_entityname FOREIGN KEY (tabid) REFERENCES vtiger_tab(tabid) ON DELETE CASCADE;CREATE TABLE vtiger_version (
    id INTEGER(11) NOT NULL AUTO_INCREMENT,
    old_version VARCHAR(30),
    current_version VARCHAR(30),
    PRIMARY KEY (id)
  ) Engine = InnoDB;CREATE TABLE vtiger_relatedlists_rb (
    entityid INTEGER(19),
    action VARCHAR(50),
    rel_table VARCHAR(200),
    rel_column VARCHAR(200),
    ref_column VARCHAR(200),
    related_crm_ids TEXT
  ) Engine = InnoDB;CREATE TABLE vtiger_activity_reminder_popup (
    reminderid INTEGER(19) NOT NULL AUTO_INCREMENT,
    semodule VARCHAR(100) NOT NULL,
    recordid INTEGER(19) NOT NULL,
    date_start DATE NOT NULL,
    time_start VARCHAR(100) NOT NULL,
    status INTEGER(2) NOT NULL,
    PRIMARY KEY (reminderid)
  ) Engine = InnoDB;CREATE TABLE vtiger_reminder_interval (
    reminder_intervalid INTEGER(19) NOT NULL AUTO_INCREMENT,
    reminder_interval VARCHAR(200) NOT NULL,
    sortorderid INTEGER(19) NOT NULL,
    presence INTEGER(1) NOT NULL,
    PRIMARY KEY (reminder_intervalid)
  ) Engine = InnoDB;CREATE TABLE vtiger_user2mergefields (
    userid INTEGER(11),
    tabid INTEGER(19),
    fieldid INTEGER(19),
    visible INTEGER(2)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_user2mergefields
ADD
  INDEX userid_tabid_idx (userid, tabid);CREATE TABLE vtiger_productcurrencyrel (
    productid INTEGER(11) NOT NULL,
    currencyid INTEGER(11) NOT NULL,
    converted_price NUMERIC(25, 2),
    actual_price NUMERIC(25, 2)
  ) Engine = InnoDB;CREATE TABLE vtiger_attachmentsfolder (
    folderid INTEGER(19) NOT NULL AUTO_INCREMENT,
    foldername VARCHAR(200) NOT NULL,
    description VARCHAR(250),
    createdby INTEGER(19) NOT NULL,
    sequence INTEGER(19) DEFAULT NULL,
    PRIMARY KEY (folderid)
  ) Engine = InnoDB;CREATE TABLE vtiger_homestuff (
    stuffid INTEGER(19) NOT NULL DEFAULT 0,
    stuffsequence INTEGER(19) NOT NULL DEFAULT 0,
    stufftype VARCHAR(100),
    userid INTEGER(19) NOT NULL,
    visible INTEGER(10) NOT NULL DEFAULT 0,
    stufftitle VARCHAR(100),
    PRIMARY KEY (stuffid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_homestuff
ADD
  INDEX stuff_stuffid_idx (stuffid);
ALTER TABLE
  vtiger_homestuff
ADD
  CONSTRAINT fk_1_vtiger_homestuff FOREIGN KEY (userid) REFERENCES vtiger_users(id) ON DELETE CASCADE;CREATE TABLE vtiger_homemodule (
    stuffid INTEGER(19) NOT NULL,
    modulename VARCHAR(100),
    maxentries INTEGER(19) NOT NULL,
    customviewid INTEGER(19) NOT NULL,
    setype VARCHAR(30) NOT NULL,
    PRIMARY KEY (stuffid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_homemodule
ADD
  INDEX stuff_stuffid_idx (stuffid);
ALTER TABLE
  vtiger_homemodule
ADD
  CONSTRAINT fk_1_vtiger_homemodule FOREIGN KEY (stuffid) REFERENCES vtiger_homestuff(stuffid) ON DELETE CASCADE;CREATE TABLE vtiger_homemoduleflds (stuffid INTEGER(19), fieldname VARCHAR(100)) Engine = InnoDB;
ALTER TABLE
  vtiger_homemoduleflds
ADD
  INDEX stuff_stuffid_idx (stuffid);
ALTER TABLE
  vtiger_homemoduleflds
ADD
  CONSTRAINT fk_1_vtiger_homemoduleflds FOREIGN KEY (stuffid) REFERENCES vtiger_homemodule(stuffid) ON DELETE CASCADE;CREATE TABLE vtiger_homerss (
    stuffid INTEGER(19) NOT NULL DEFAULT 0,
    url VARCHAR(100),
    maxentries INTEGER(19) NOT NULL,
    PRIMARY KEY (stuffid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_homerss
ADD
  INDEX stuff_stuffid_idx (stuffid);
ALTER TABLE
  vtiger_homerss
ADD
  CONSTRAINT fk_1_vtiger_homerss FOREIGN KEY (stuffid) REFERENCES vtiger_homestuff(stuffid) ON DELETE CASCADE;CREATE TABLE vtiger_homedashbd (
    stuffid INTEGER(19) NOT NULL DEFAULT 0,
    dashbdname VARCHAR(100),
    dashbdtype VARCHAR(100),
    PRIMARY KEY (stuffid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_homedashbd
ADD
  INDEX stuff_stuffid_idx (stuffid);
ALTER TABLE
  vtiger_homedashbd
ADD
  CONSTRAINT fk_1_vtiger_homedashbd FOREIGN KEY (stuffid) REFERENCES vtiger_homestuff(stuffid) ON DELETE CASCADE;CREATE TABLE vtiger_homedefault (
    stuffid INTEGER(19) NOT NULL DEFAULT 0,
    hometype VARCHAR(30) NOT NULL,
    maxentries INTEGER(19),
    setype VARCHAR(30),
    PRIMARY KEY (stuffid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_homedefault
ADD
  INDEX stuff_stuffid_idx (stuffid);
ALTER TABLE
  vtiger_homedefault
ADD
  CONSTRAINT fk_1_vtiger_homedefault FOREIGN KEY (stuffid) REFERENCES vtiger_homestuff(stuffid) ON DELETE CASCADE;CREATE TABLE vtiger_ws_fieldtype (
    fieldtypeid INTEGER(19) NOT NULL AUTO_INCREMENT,
    uitype VARCHAR(30) NOT NULL,
    fieldtype VARCHAR(200) NOT NULL,
    PRIMARY KEY (fieldtypeid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_ws_fieldtype
ADD
  UNIQUE INDEX uitype_idx (uitype);CREATE TABLE vtiger_ws_referencetype (
    fieldtypeid INTEGER(19) NOT NULL,
    type VARCHAR(25) NOT NULL,
    PRIMARY KEY (fieldtypeid, type)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_ws_referencetype
ADD
  CONSTRAINT `fk_1_vtiger_referencetype` FOREIGN KEY (`fieldtypeid`) REFERENCES `vtiger_ws_fieldtype` (`fieldtypeid`) ON DELETE CASCADE;CREATE TABLE vtiger_ws_userauthtoken (
    userid INTEGER(19) NOT NULL,
    token VARCHAR(36) NOT NULL,
    expiretime INTEGER(19) NOT NULL,
    PRIMARY KEY (userid, expiretime)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_ws_userauthtoken
ADD
  UNIQUE INDEX userid_idx (userid);CREATE TABLE vtiger_ws_entity (
    id INTEGER(11) NOT NULL AUTO_INCREMENT,
    name VARCHAR(25) NOT NULL,
    handler_path VARCHAR(255) NOT NULL,
    handler_class VARCHAR(64) NOT NULL,
    ismodule INTEGER(3) NOT NULL,
    PRIMARY KEY (id)
  ) Engine = InnoDB;CREATE TABLE vtiger_ws_entity_tables (
    webservice_entity_id INTEGER(11) NOT NULL,
    table_name VARCHAR(50) NOT NULL,
    PRIMARY KEY (webservice_entity_id, table_name)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_ws_entity_tables
ADD
  CONSTRAINT `fk_1_vtiger_ws_actor_tables` FOREIGN KEY (`webservice_entity_id`) REFERENCES `vtiger_ws_entity` (`id`) ON DELETE CASCADE;CREATE TABLE vtiger_ws_entity_fieldtype (
    fieldtypeid INTEGER(19) NOT NULL AUTO_INCREMENT,
    table_name VARCHAR(50) NOT NULL,
    field_name VARCHAR(50) NOT NULL,
    fieldtype VARCHAR(200) NOT NULL,
    PRIMARY KEY (fieldtypeid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_ws_entity_fieldtype
ADD
  UNIQUE INDEX vtiger_idx_1_tablename_fieldname (table_name, field_name);CREATE TABLE vtiger_ws_entity_referencetype (
    fieldtypeid INTEGER(19) NOT NULL,
    type VARCHAR(25) NOT NULL,
    PRIMARY KEY (fieldtypeid, type)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_ws_entity_referencetype
ADD
  CONSTRAINT `vtiger_fk_1_actors_referencetype` FOREIGN KEY (`fieldtypeid`) REFERENCES `vtiger_ws_entity_fieldtype` (`fieldtypeid`) ON DELETE CASCADE;CREATE TABLE vtiger_ws_operation (
    operationid INTEGER(11) NOT NULL AUTO_INCREMENT,
    name VARCHAR(128) NOT NULL,
    handler_path VARCHAR(255) NOT NULL,
    handler_method VARCHAR(64) NOT NULL,
    type VARCHAR(8) NOT NULL,
    prelogin INTEGER(3) NOT NULL,
    PRIMARY KEY (operationid)
  ) Engine = InnoDB;CREATE TABLE vtiger_ws_operation_parameters (
    operationid INTEGER(11) NOT NULL AUTO_INCREMENT,
    name VARCHAR(128) NOT NULL,
    type VARCHAR(64) NOT NULL,
    sequence INTEGER(11) NOT NULL,
    PRIMARY KEY (operationid, name)
  ) Engine = InnoDB;CREATE TABLE vtiger_ws_entity_name (
    entity_id INTEGER(11) NOT NULL,
    name_fields VARCHAR(50) NOT NULL,
    index_field VARCHAR(50) NOT NULL,
    table_name VARCHAR(50) NOT NULL,
    PRIMARY KEY (entity_id)
  ) Engine = InnoDB;CREATE TABLE vtiger_eventhandlers (
    eventhandler_id INTEGER NOT NULL AUTO_INCREMENT,
    event_name VARCHAR(100) NOT NULL,
    handler_path VARCHAR(400) NOT NULL,
    handler_class VARCHAR(100) NOT NULL,
    cond TEXT,
    is_active INTEGER(1) NOT NULL,
    dependent_on VARCHAR(255) DEFAULT '[]',
    PRIMARY KEY (eventhandler_id, event_name, handler_class)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_eventhandlers
ADD
  UNIQUE INDEX eventhandler_idx (eventhandler_id);CREATE TABLE vtiger_eventhandler_module (
    eventhandler_module_id INTEGER NOT NULL AUTO_INCREMENT,
    module_name VARCHAR(100),
    handler_class VARCHAR(100),
    PRIMARY KEY (eventhandler_module_id)
  ) Engine = InnoDB;CREATE TABLE vtiger_mailscanner (
    scannerid INTEGER NOT NULL AUTO_INCREMENT,
    scannername VARCHAR(30),
    server VARCHAR(100),
    protocol VARCHAR(10),
    username VARCHAR(255),
    password VARCHAR(255),
    ssltype VARCHAR(10),
    sslmethod VARCHAR(30),
    connecturl VARCHAR(255),
    searchfor VARCHAR(10),
    markas VARCHAR(10),
    isvalid INTEGER(1),
    scanfrom VARCHAR(10) DEFAULT 'ALL',
    PRIMARY KEY (scannerid)
  ) Engine = InnoDB;CREATE TABLE vtiger_mailscanner_ids (
    scannerid INTEGER,
    messageid TEXT,
    crmid INTEGER,
    refids TEXT
  ) Engine = InnoDB;CREATE TABLE vtiger_mailscanner_folders (
    folderid INTEGER NOT NULL AUTO_INCREMENT,
    scannerid INTEGER,
    foldername VARCHAR(255),
    lastscan VARCHAR(30),
    rescan INTEGER(1),
    enabled INTEGER(1),
    PRIMARY KEY (folderid)
  ) Engine = InnoDB;CREATE TABLE vtiger_mailscanner_rules (
    ruleid INTEGER NOT NULL AUTO_INCREMENT,
    scannerid INTEGER,
    fromaddress VARCHAR(255),
    toaddress VARCHAR(255),
    subjectop VARCHAR(20),
    subject VARCHAR(255),
    bodyop VARCHAR(20),
    body VARCHAR(255),
    matchusing VARCHAR(5),
    sequence INTEGER,
    PRIMARY KEY (ruleid)
  ) Engine = InnoDB;CREATE TABLE vtiger_mailscanner_actions (
    actionid INTEGER NOT NULL AUTO_INCREMENT,
    scannerid INTEGER,
    actiontype VARCHAR(10),
    module VARCHAR(30),
    lookup VARCHAR(30),
    sequence INTEGER,
    PRIMARY KEY (actionid)
  ) Engine = InnoDB;CREATE TABLE vtiger_mailscanner_ruleactions (ruleid INTEGER, actionid INTEGER) Engine = InnoDB;CREATE TABLE vtiger_invoice_recurring_info (
    salesorderid INTEGER,
    recurring_frequency VARCHAR(200),
    start_period DATE,
    end_period DATE,
    last_recurring_date DATE DEFAULT NULL,
    payment_duration VARCHAR(200),
    invoice_status VARCHAR(200)
  ) Engine = InnoDB;CREATE TABLE vtiger_recurring_frequency (
    recurring_frequency_id INTEGER,
    recurring_frequency VARCHAR(200),
    sortorderid INTEGER,
    presence INTEGER
  ) Engine = InnoDB;CREATE TABLE vtiger_payment_duration (
    payment_duration_id INTEGER,
    payment_duration VARCHAR(200),
    sortorderid INTEGER,
    presence INTEGER
  ) Engine = InnoDB;CREATE TABLE com_vtiger_workflows (
    workflow_id INTEGER NOT NULL AUTO_INCREMENT,
    module_name VARCHAR(100),
    summary VARCHAR(400) NOT NULL,
    test TEXT,
    execution_condition INTEGER NOT NULL,
    defaultworkflow INTEGER(1),
    type VARCHAR(255),
    filtersavedinnew INTEGER(1),
    schtypeid INTEGER(10),
    schdayofmonth VARCHAR(100),
    schdayofweek VARCHAR(100),
    schannualdates VARCHAR(100),
    schtime VARCHAR(50),
    nexttrigger_time DATETIME,
    status INTEGER(1),
    workflowname VARCHAR(100),
    PRIMARY KEY (workflow_id)
  ) Engine = InnoDB;
ALTER TABLE
  com_vtiger_workflows
ADD
  UNIQUE INDEX com_vtiger_workflows_idx (workflow_id);CREATE TABLE com_vtiger_workflow_activatedonce (
    workflow_id INTEGER NOT NULL,
    entity_id INTEGER NOT NULL,
    PRIMARY KEY (workflow_id, entity_id)
  ) Engine = InnoDB;CREATE TABLE com_vtiger_workflowtasks (
    task_id INTEGER NOT NULL AUTO_INCREMENT,
    workflow_id INTEGER,
    summary VARCHAR(400) NOT NULL,
    task TEXT,
    PRIMARY KEY (task_id)
  ) Engine = InnoDB;
ALTER TABLE
  com_vtiger_workflowtasks
ADD
  UNIQUE INDEX com_vtiger_workflowtasks_idx (task_id);CREATE TABLE com_vtiger_workflowtask_queue (
    task_id INTEGER,
    entity_id VARCHAR(100),
    do_after INTEGER,
    relatedinfo VARCHAR(255)
  ) Engine = InnoDB;
ALTER TABLE
  com_vtiger_workflowtask_queue
ADD
  UNIQUE INDEX com_vtiger_workflowtask_queue_idx (task_id, entity_id);CREATE TABLE com_vtiger_workflowtasks_entitymethod (
    workflowtasks_entitymethod_id INTEGER NOT NULL,
    module_name VARCHAR(100),
    method_name VARCHAR(100),
    function_path VARCHAR(400),
    function_name VARCHAR(100),
    PRIMARY KEY (workflowtasks_entitymethod_id)
  ) Engine = InnoDB;
ALTER TABLE
  com_vtiger_workflowtasks_entitymethod
ADD
  UNIQUE INDEX com_vtiger_workflowtasks_entitymethod_idx (workflowtasks_entitymethod_id);CREATE TABLE com_vtiger_workflowtemplates (
    template_id INTEGER NOT NULL AUTO_INCREMENT,
    module_name VARCHAR(100),
    title VARCHAR(400),
    template TEXT,
    PRIMARY KEY (template_id)
  ) Engine = InnoDB;CREATE TABLE com_vtiger_workflow_tasktypes (
    id INTEGER(11) NOT NULL,
    tasktypename VARCHAR(255) NOT NULL,
    label VARCHAR(255),
    classname VARCHAR(255),
    classpath VARCHAR(255),
    templatepath VARCHAR(255),
    modules VARCHAR(500),
    sourcemodule VARCHAR(255)
  ) Engine = InnoDB;CREATE TABLE vtiger_asteriskextensions (
    userid INTEGER(11),
    asterisk_extension VARCHAR(50),
    use_asterisk VARCHAR(3)
  ) Engine = InnoDB;CREATE TABLE vtiger_asterisk (
    server VARCHAR(30),
    port VARCHAR(30),
    username VARCHAR(50),
    password VARCHAR(50),
    version VARCHAR(50)
  ) Engine = InnoDB;CREATE TABLE vtiger_asteriskincomingcalls (
    from_number VARCHAR(50),
    from_name VARCHAR(50),
    to_number VARCHAR(50),
    callertype VARCHAR(30),
    flag INTEGER(19),
    timer INTEGER(19),
    refuid VARCHAR(255)
  ) Engine = InnoDB;CREATE TABLE vtiger_asteriskincomingevents (
    uid VARCHAR(255) NOT NULL,
    channel VARCHAR(100),
    from_number BIGINT(20),
    from_name VARCHAR(100),
    to_number BIGINT(20),
    callertype VARCHAR(100),
    timer INTEGER(20),
    flag VARCHAR(3),
    pbxrecordid INTEGER(19),
    relcrmid INTEGER(19),
    PRIMARY KEY (uid)
  );CREATE TABLE vtiger_modentity_num (
    num_id INTEGER(19) NOT NULL,
    semodule VARCHAR(50) NOT NULL,
    prefix VARCHAR(50) NOT NULL DEFAULT '',
    start_id VARCHAR(50) NOT NULL,
    cur_id VARCHAR(50) NOT NULL,
    active VARCHAR(2) NOT NULL,
    PRIMARY KEY (num_id)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_modentity_num
ADD
  UNIQUE INDEX num_idx (num_id);
ALTER TABLE
  vtiger_modentity_num
ADD
  INDEX semodule_active_idx (semodule, active);CREATE TABLE vtiger_language (
    id INTEGER NOT NULL AUTO_INCREMENT,
    name VARCHAR(50),
    prefix VARCHAR(10),
    label VARCHAR(30),
    lastupdated DATETIME,
    sequence INTEGER,
    isdefault INTEGER(1),
    active INTEGER(1),
    PRIMARY KEY (id)
  ) Engine = InnoDB;CREATE TABLE vtiger_crmentityrel (
    crmid INTEGER NOT NULL,
    module VARCHAR(100) NOT NULL,
    relcrmid INTEGER NOT NULL,
    relmodule VARCHAR(100) NOT NULL
  ) Engine = InnoDB;CREATE TABLE vtiger_fieldmodulerel (
    fieldid INTEGER NOT NULL,
    module VARCHAR(100) NOT NULL,
    relmodule VARCHAR(100) NOT NULL,
    status VARCHAR(10) DEFAULT NULL,
    sequence INTEGER DEFAULT NULL
  ) Engine = InnoDB;CREATE TABLE vtiger_links (
    linkid INTEGER NOT NULL,
    tabid INTEGER,
    linktype VARCHAR(50),
    linklabel VARCHAR(50),
    linkurl VARCHAR(255),
    linkicon VARCHAR(100),
    sequence INTEGER,
    handler_path VARCHAR(128) DEFAULT NULL,
    handler_class VARCHAR(50) DEFAULT NULL,
    handler VARCHAR(50) DEFAULT NULL,
    parent_link INTEGER(19),
    PRIMARY KEY (linkid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_links
ADD
  INDEX link_tabidtype_idx (tabid, linktype);CREATE TABLE vtiger_settings_blocks (
    blockid INTEGER(19) NOT NULL,
    label VARCHAR(250),
    sequence INTEGER(19),
    PRIMARY KEY (blockid)
  ) Engine = InnoDB;CREATE TABLE vtiger_settings_field (
    fieldid INTEGER(19) NOT NULL,
    blockid INTEGER(19),
    name VARCHAR(250),
    iconpath VARCHAR(300),
    description TEXT,
    linkto TEXT,
    sequence INTEGER(19),
    active INTEGER(19) DEFAULT 0,
    PRIMARY KEY (fieldid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_settings_field
ADD
  CONSTRAINT fk_1_vtiger_settings_field FOREIGN KEY (blockid) REFERENCES vtiger_settings_blocks(blockid) ON DELETE CASCADE;CREATE TABLE vtiger_email_access (
    crmid INTEGER,
    mailid INTEGER,
    accessdate DATE,
    accesstime DATETIME
  ) Engine = InnoDB;CREATE TABLE vtiger_email_track (
    crmid INTEGER,
    mailid INTEGER,
    access_count INTEGER
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_email_track
ADD
  UNIQUE INDEX link_tabidtype_idx (crmid, mailid);CREATE TABLE vtiger_reportsharing (
    reportid INTEGER(19) NOT NULL,
    shareid INTEGER(19) NOT NULL,
    setype VARCHAR(200) NOT NULL
  ) Engine = InnoDB;CREATE TABLE vtiger_reportfilters (
    filterid INTEGER(19) NOT NULL,
    name VARCHAR(200) NOT NULL
  ) Engine = InnoDB;CREATE TABLE vtiger_notebook_contents (
    userid INTEGER(19) NOT NULL,
    notebookid INTEGER(19) NOT NULL,
    contents TEXT
  ) Engine = InnoDB;CREATE TABLE vtiger_inventorysubproductrel (
    id INTEGER(19) NOT NULL,
    sequence_no INTEGER(10) NOT NULL,
    productid INTEGER(19) NOT NULL,
    quantity INTEGER(19) DEFAULT 1
  ) Engine = InnoDB;CREATE TABLE vtiger_activitycf (
    activityid INTEGER(19) NOT NULL DEFAULT 0,
    PRIMARY KEY (activityid)
  ) Engine = InnoDB;CREATE TABLE vtiger_user_module_preferences (
    userid INTEGER(19) NOT NULL,
    tabid INTEGER(19) NOT NULL,
    default_cvid INTEGER(19) NOT NULL,
    PRIMARY KEY (userid, tabid)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_user_module_preferences
ADD
  CONSTRAINT fk_2_vtiger_user_module_preferences FOREIGN KEY (tabid) REFERENCES vtiger_tab(tabid) ON DELETE CASCADE ON UPDATE CASCADE;CREATE TABLE vtiger_home_layout (
    userid INTEGER(19) NOT NULL,
    layout INTEGER(19) NOT NULL DEFAULT 4,
    PRIMARY KEY (userid)
  ) Engine = InnoDB;CREATE TABLE vtiger_notescf (
    notesid INTEGER(19) NOT NULL DEFAULT 0,
    PRIMARY KEY (notesid)
  ) Engine = InnoDB;CREATE TABLE vtiger_currencies (
    currencyid INTEGER(19) NOT NULL AUTO_INCREMENT,
    currency_name VARCHAR(200),
    currency_code VARCHAR(50),
    currency_symbol VARCHAR(11),
    PRIMARY KEY (currencyid)
  ) Engine = InnoDB;CREATE TABLE vtiger_tab_info (
    tabid INTEGER(19),
    prefname VARCHAR(256),
    prefvalue VARCHAR(256)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_tab_info
ADD
  CONSTRAINT fk_1_vtiger_tab_info FOREIGN KEY (tabid) REFERENCES vtiger_tab(tabid) ON DELETE CASCADE ON UPDATE CASCADE;CREATE TABLE vtiger_campaignrelstatus (
    campaignrelstatusid INTEGER(19),
    campaignrelstatus VARCHAR(256),
    sortorderid INTEGER(19),
    presence INTEGER(19)
  ) Engine = InnoDB;CREATE TABLE vtiger_campaignaccountrel (
    campaignid INTEGER(19),
    accountid INTEGER(19),
    campaignrelstatusid INTEGER(19)
  ) Engine = InnoDB;CREATE TABLE vtiger_customerportal_tabs (
    tabid INTEGER(19) NOT NULL,
    visible INTEGER(1) DEFAULT 1,
    sequence INTEGER(1) DEFAULT NULL,
    PRIMARY KEY (tabid)
  ) Engine = InnoDB;CREATE TABLE vtiger_customerportal_prefs (
    tabid INTEGER(19) NOT NULL,
    prefkey VARCHAR(100) NOT NULL,
    prefvalue INTEGER(20) DEFAULT NULL,
    PRIMARY KEY (tabid, prefkey)
  ) Engine = InnoDB;CREATE TABLE vtiger_relcriteria_grouping (
    groupid INTEGER(11) NOT NULL,
    queryid INTEGER(19) NOT NULL,
    group_condition VARCHAR(256),
    condition_expression TEXT,
    PRIMARY KEY (groupid, queryid)
  ) Engine = InnoDB;CREATE TABLE vtiger_scheduled_reports (
    reportid INTEGER NOT NULL,
    recipients TEXT,
    schedule TEXT,
    format VARCHAR(10),
    next_trigger_time TIMESTAMP,
    PRIMARY KEY (reportid)
  ) Engine = InnoDB;CREATE TABLE vtiger_ws_fieldinfo (
    id VARCHAR(64) NOT NULL,
    property_name VARCHAR(32),
    property_value VARCHAR(64),
    PRIMARY KEY (id)
  ) Engine = InnoDB;CREATE TABLE vtiger_cvadvfilter_grouping (
    groupid INTEGER(11) NOT NULL,
    cvid INTEGER(19) NOT NULL,
    group_condition VARCHAR(255),
    condition_expression TEXT,
    PRIMARY KEY (groupid, cvid)
  ) Engine = InnoDB;CREATE TABLE vtiger_picklist_dependency (
    id INTEGER(11) NOT NULL,
    tabid INTEGER(19) NOT NULL,
    sourcefield VARCHAR(255),
    targetfield VARCHAR(255),
    sourcevalue VARCHAR(100),
    targetvalues TEXT,
    criteria TEXT,
    PRIMARY KEY (id)
  ) Engine = InnoDB;CREATE TABLE vtiger_currency_grouping_pattern (
    currency_grouping_patternid INTEGER(19) NOT NULL AUTO_INCREMENT,
    currency_grouping_pattern VARCHAR(200) NOT NULL,
    sortorderid INTEGER(19) NOT NULL DEFAULT 0,
    presence INTEGER(1) NOT NULL DEFAULT 1,
    PRIMARY KEY (currency_grouping_patternid)
  ) Engine = InnoDB;CREATE TABLE vtiger_currency_decimal_separator (
    currency_decimal_separatorid INTEGER(19) NOT NULL AUTO_INCREMENT,
    currency_decimal_separator VARCHAR(2) NOT NULL,
    sortorderid INTEGER(19) NOT NULL DEFAULT 0,
    presence INTEGER(1) NOT NULL DEFAULT 1,
    PRIMARY KEY (currency_decimal_separatorid)
  ) Engine = InnoDB;CREATE TABLE vtiger_currency_grouping_separator (
    currency_grouping_separatorid INTEGER(19) NOT NULL AUTO_INCREMENT,
    currency_grouping_separator VARCHAR(2) NOT NULL,
    sortorderid INTEGER(19) NOT NULL DEFAULT 0,
    presence INTEGER(1) NOT NULL DEFAULT 1,
    PRIMARY KEY (currency_grouping_separatorid)
  ) Engine = InnoDB;CREATE TABLE vtiger_currency_symbol_placement (
    currency_symbol_placementid INTEGER(19) NOT NULL AUTO_INCREMENT,
    currency_symbol_placement VARCHAR(30) NOT NULL,
    sortorderid INTEGER(19) NOT NULL DEFAULT 0,
    presence INTEGER(1) NOT NULL DEFAULT 1,
    PRIMARY KEY (currency_symbol_placementid)
  ) Engine = InnoDB;CREATE TABLE vtiger_time_zone (
    time_zoneid INTEGER(19) NOT NULL AUTO_INCREMENT,
    time_zone VARCHAR(200) NOT NULL,
    sortorderid INTEGER(19) NOT NULL DEFAULT 0,
    presence INTEGER(1) NOT NULL DEFAULT 1,
    PRIMARY KEY (time_zoneid)
  ) Engine = InnoDB;CREATE TABLE vtiger_homereportchart (
    stuffid INTEGER(11) NOT NULL,
    reportid INTEGER(19),
    reportcharttype VARCHAR(100),
    PRIMARY KEY (stuffid)
  ) Engine = InnoDB;CREATE TABLE vtiger_reportgroupbycolumn (
    reportid INTEGER(19),
    sortid INTEGER(19),
    sortcolname VARCHAR(250),
    dategroupbycriteria VARCHAR(250)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_reportgroupbycolumn
ADD
  CONSTRAINT fk_1_vtiger_reportgroupbycolumn FOREIGN KEY (reportid) REFERENCES vtiger_report(reportid) ON DELETE CASCADE;CREATE TABLE vtiger_crmsetup (userid INTEGER(11), setup_status INTEGER(2)) Engine = InnoDB;CREATE TABLE vtiger_feedback (
    userid INTEGER(19),
    dontshow VARCHAR(19) DEFAULT 'false'
  ) Engine = InnoDB;CREATE TABLE vtiger_shareduserinfo (
    userid INTEGER(19) NOT NULL DEFAULT 0,
    shareduserid INTEGER(19) NOT NULL DEFAULT 0,
    color VARCHAR(50),
    visible INTEGER(19) DEFAULT 1
  ) Engine = InnoDB;CREATE TABLE vtiger_calendar_default_activitytypes (
    id INTEGER(19) NOT NULL,
    module VARCHAR(50),
    fieldname VARCHAR(50),
    defaultcolor VARCHAR(50),
    isdefault INTEGER(11) DEFAULT 1,
    conditions VARCHAR(255),
    PRIMARY KEY (id)
  ) Engine = InnoDB;CREATE TABLE vtiger_calendar_user_activitytypes (
    id INTEGER(19) NOT NULL,
    defaultid INTEGER(19),
    userid INTEGER(19),
    color VARCHAR(50),
    visible INTEGER(19) DEFAULT 1,
    PRIMARY KEY (id)
  ) Engine = InnoDB;CREATE TABLE vtiger_dashboard_tabs (
    id INTEGER(19) NOT NULL,
    tabname VARCHAR(50),
    isdefault INTEGER(1) DEFAULT 0,
    sequence INTEGER(5) DEFAULT 2,
    appname VARCHAR(20),
    modulename VARCHAR(50),
    userid INTEGER(11),
    PRIMARY KEY (id)
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_dashboard_tabs
ADD
  CONSTRAINT vtiger_dashboard_tabs_ibfk_1 FOREIGN KEY (userid) REFERENCES vtiger_users(id) ON DELETE CASCADE;CREATE TABLE vtiger_app2tab (
    tabid INTEGER(11),
    appname VARCHAR(20),
    sequence INTEGER(19),
    visible INTEGER(3) DEFAULT 1
  ) Engine = InnoDB;
ALTER TABLE
  vtiger_app2tab
ADD
  CONSTRAINT vtiger_app2tab_fk_tab FOREIGN KEY (tabid) REFERENCES vtiger_tab(tabid) ON DELETE CASCADE
